package eu.transkribus.core.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.exceptions.InvalidUserInputException;

import java.util.regex.Pattern;

public class UserInputChecker {
	private static final Logger logger = LoggerFactory.getLogger(UserInputChecker.class);
	public static final String EMAIL_PATTERN_STR =
			"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"
					+ "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

	public static void checkCollectionName(String collName) throws InvalidUserInputException {
		if (collName == null || collName.trim().length() < 1) {
			String msg = "Collection name is empty!";
			throw new InvalidUserInputException(msg);
		}
	}

	/**
	 * Validate an email address string
	 * @param email address string
	 * @param checkMxRecord if true, do a check that there is at least one MX record on the email address' domain
	 * @return true, if email has a valid format and, optionally if there is an MX record for the domain
	 */
	public static boolean isValidEmail(final String email, boolean checkMxRecord) {
		if(StringUtils.isBlank(email)) {
			return false;
		}
		if(!Pattern.matches(EMAIL_PATTERN_STR, email.toLowerCase())) {
			return false;
		}
		if((checkMxRecord)) {
			final int nrOfMailServers = EMailMxLookup.doLookupAddress(email);
			return nrOfMailServers > 0;
		}
		return true;
	}
}
