package eu.transkribus.core.util;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EMailMxLookup {
	private static final Logger logger = LoggerFactory.getLogger(EMailMxLookup.class);
	
	private final static String AT = "@";
	private final static String MX = "MX";
	
	public static int doLookupAddress(final String emailAddress) {
		if(StringUtils.isEmpty(emailAddress) || !emailAddress.contains(AT)) {
			throw new IllegalArgumentException("Not an email address: " + emailAddress);
		}
		return doLookupHost(emailAddress.split(AT)[1]);
	}
	
	/**
	 * @param hostName
	 * @return number of mail servers registered to the domain name
	 */
	public static int doLookupHost(final String hostName) {
		Hashtable<String, String> env = new Hashtable<>();
		try {
			env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
			DirContext ictx = new InitialDirContext(env);
			Attributes attrs = ictx.getAttributes(hostName, new String[]{MX});
			Attribute attr = attrs.get(MX);
			if (attr == null) {
				return 0;
			}
			return attr.size();
		} catch (NamingException e) {
			//e.g. NameNotFoundException is thrown when the hostname lookup fails
			logger.info("Could not look up MX record for '{}': {} - {}", hostName, e.getClass().getCanonicalName(), e.getMessage());
			return 0;
		}
	}
}