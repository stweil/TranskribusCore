package eu.transkribus.core.rest;

public class JobConst {
	public static final String SEP = ",";
	
	public static final String JOBS_PACKAGE = "eu.transkribus.persistence.jobs.";
	
	public static final String STATE_CREATE = "create";
	public static final String STATE_HTR = "htr";
	public static final String STATE_FINISH = "finish";
	public static final String STATE_DOWNLOAD = "download";
	public static final String STATE_OCR = "ocr";
	public static final String STATE_INGEST = "ingest";
	
	public static final String PROP_DOC_ID = "docId";
	public static final String PROP_NEW_DOC_ID = "newDocId";
	public static final String PROP_DOC_DESCS = "docDescs"; // property for list of DocumentSelectionDescriptor's
	public static final String PROP_PAGE_NR = "pageNr";
	public static final String PROP_JOB_ID = "jobId";
	public static final String PROP_USER_ID = "userId";
	public static final String PROP_USER_NAME = "userName";
	public static final String PROP_USER_EMAIL = "userEmail";
	public static final String PROP_USER_ROLES_CSV = "userRoles";
	public static final String PROP_SESSION_ID = "sessionId";
	public static final String PROP_COLLECTION_ID = "colId";
	public static final String PROP_TITLE = "title";
	public static final String PROP_TRANSCRIPT_ID = "transcriptId";
	public static final String PROP_REG_IDS = "regIds";
	public static final String PROP_TRANSCRIPTS = "transcripts";
	
	public static final String PROP_TRAIN_DESCS = "trainDescs"; // property for list of DocumentSelectionDescriptor's for training
	public static final String PROP_VAL_DESCS = "valDescs"; // property for list of DocumentSelectionDescriptor's for validation
	public static final String PROP_TEST_DESCS = "testDescs"; // property for list of DocumentSelectionDescriptor's for test
	
	public static final String PROP_TRAIN_DOC_SELS = "trainDocSels"; // property for list of DocSelection's for training
	public static final String PROP_VAL_DOC_SELS = "valDocSels"; // property for list of DocSelection's for validation
	public static final String PROP_TEST_DOC_SELS = "testDocSels"; // property for list of DocSelection's for test	
	
	public static final String PROP_TSIDS = "tsIds"; // property for transcription ids
//	public static final String PROP_TRAIN_TSIDS = "trainTsIds";
//	public static final String PROP_VAL_TSIDS = "valTsIds";
//	public static final String PROP_TEST_TSDIS = "testTsIds";
	
	public static final String PROP_STATE = "state";
	public static final String PROP_MODELNAME = "modelName";
	public static final String PROP_MODEL_ID = "modelId";
	public static final String PROP_SOURCE_TEXT_PATH = "sourceTextPath";

	public static final String PROP_PRINTED_MODEL_ID = "printedModelId";
	public static final String PROP_DESCRIPTION = "description";
	public static final String PROP_PAGES = "pages";
	public static final String PROP_DOC_IDS = "docIds";
	public static final String PROP_PATH = "path";
	public static final String PROP_DEALOG_DOC_ID = "dealogDocId";
	public static final String PROP_METS_PATH = "metsPath";
	public static final String PROP_IIIF_PATH = "iiifPath";
	public static final String PROP_DO_DELETE_IMPORT_SOURCE = "doDeleteImportSource";
	
	public static final String PROP_DICTNAME = "dictName";
	public static final String PROP_TEMP_DICTNAME = "tempDictName";
	public static final String PROP_LANGUAGE_MODEL = "languageModel";
	public static final String PROP_PRINTED_LANGUAGE_MODEL = "printedLanguageModel";
	public static final String PROP_TRAIN_DATA_DICT_VALUE = "trainDataDictionary";
	public static final String PROP_TRAIN_DATA_LM_VALUE = "trainDataLanguageModel";
	public static final String PROP_NUM_EPOCHS = "numEpochs";
	public static final String PROP_LEARNING_RATE = "learningRate";
	public static final String PROP_NOISE = "noise";
	public static final String PROP_NR_OF_THREADS = "nrOfThreads";
	public static final String PROP_TRAIN_SIZE_PER_EPOCH = "trainSizePerEpoch";
	public static final String PROP_BASE_MODEL = "baseModel";
	public static final String PROP_EARLY_STOPPING = "earlyStopping";
	public static final String PROP_HTR_OMIT_LINES_BY_TAG = "omitLinesByTag";
	public static final String PROP_IMG_TYPE = "imgType";
	
	// for reversing text in training/recognition:
	public static final String PROP_REVERSE_TEXT = "reverseText";
	public static final String PROP_REVERSE_TEXT_EXCLUDE_DIGITS = PROP_REVERSE_TEXT+"ExcludeDigits";
	public static final String PROP_REVERSE_TEXT_TAG_EXCEPTIONS = PROP_REVERSE_TEXT+"TagExceptions";
	
	//for training of custom tags
	public static final String PROP_CUSTOM_TAG_TRAINING = "customTagTraining";
	public static final String PROP_CUSTOM_ABBREVS_TRAINING = "customAbbrevsTraining";
	public static final String PROP_SHORT_FORM = "shortForm";
	public static final String PROP_TRAIN_PROPERTIES = "trainProperties";
	public static final String PROP_CUSTOM_TAG_MAP = "customTagMap";
	public static final String PROP_PROPERTIES_MAP = "customPropertiesMap";
	public static final String PROP_CUSTOM_TAG_TRAINING_TAG_SELECTION = PROP_CUSTOM_TAG_TRAINING+"TagSelection";
	
	/**
	 * Use {@link #PROP_PARAMETERS} for consistency
	 */
	@Deprecated
	public static final String PROP_CONFIG = "config";
	
	public static final String PROP_LA_CONFIG = "laConfig";
	
	public static final String PROP_DO_BLOCK_SEG = "doBlockSeg";
	public static final String PROP_DO_LINE_SEG = "doLineSeg";
	public static final String PROP_DO_WORD_SEG = "doWordSeg";
	public static final String PROP_DO_POLYGON_TO_BASELINE = "doPolygonToBaseline";
	public static final String PROP_DO_BASELINE_TO_POLYGON = "doBaselineToPolygon";
	public static final String PROP_STRUCTURES = "doStructures";
	public static final String PROP_DO_NOT_DELETE_WORKDIR = "doNotDeleteWorkDir";
	
	public static final String PROP_DO_SYNCHRONIZE = "doSynchronisation";
	
	public static final String PROP_REMOVE_LINE_BREAKS = "removeLineBreaks";
	public static final String PROP_PERFORM_LAYOUT_ANALYSIS = "performLayoutAnalysis";
	public static final String PROP_T2I_SKIP_WORD = "skip_word";
	public static final String PROP_T2I_SKIP_BASELINE = "skip_bl";
	public static final String PROP_T2I_JUMP_BASELINE = "jump_bl";
	public static final String PROP_T2I_HYPHEN = "hyphen";
	
	public static final String PROP_KEEP_ORIGINAL_LINE_POLYGONS = "keepOriginalLinePolygons";
	public static final String PROP_DO_LINE_POLYGON_SIMPLIFICATION = "doLinePolygonSimplification";
	public static final String PROP_USE_EXISTING_LINE_POLYGONS = "useExistingLinePolygons";
	public static final String PROP_DO_STORE_CONFMATS = "doStoreConfMats";
	public static final String PROP_CLEAR_LINES = "clearLines";
	public static final String PROP_BATCH_SIZE = "batchSize";
	public static final String PROP_B2P_BACKEND = "b2pBackend";
	// those two props determine are used for KWS indexing:
	public static final String PROP_WRITE_KWS_INDEX = "writeKwsIndex";
	public static final String PROP_N_BEST = "nBest";
	public static final String PROP_WRITE_LINE_CONF_SCORE = "writeLineConfScore";
	public static final String PROP_WRITE_WORD_CONF_SCORES = "writeWordConfScores";
	
	public static final String PROP_TABLE_TEMPLATE_ID = "templateId";

	public static final String PROP_SOLR_URL = "solrUrl";

	public static final String PROP_ADDITIONAL_COL_IDS = "additionalColIds";

	public static final String PROP_EXPORT_OPTIONS = "options";

	public static final String PROP_PARAMETERS = "parameters";
	
	//kws
	public static final String PROP_QUERY = "query";
	/**
	 * kws result is too big for the result-column. So this goes into  the job data clob
	 */
	public static final String PROP_RESULT = "result";
	public static final String PROP_THRESHOLD = "threshold";
	public static final String PROP_EDIT_STATUS = "editStatus";
	public static final String PROP_IS_CASE_SENSITIVE = "caseSensitive";
	public static final String PROP_IS_EXPERT = "expert";
	public static final String PROP_MAX_NR_OF_HITS = "maxNrOfHits";
	public static final String PROP_DO_PARTIAL_MATCHING = "partialMatching";
	public static final String PROP_CUSTOM_PROP_MAP = "customPropMap";
	
	public static final String PROP_KEY = "key";
	public static final String PROP_IMG_FILE = "imgKey";

	public static final String PROP_TITLE_PREFIX = "titlePrefix";

	public static final String PROP_AUTHORITY = "authority";
	public static final String PROP_HIERARCHY = "hierarchy";
	public static final String PROP_BACKLINK = "backlink";
	public static final String PROP_EXTID = "extId";
	public static final String PROP_FROMDATE = "fromDate";
	public static final String PROP_TODATE = "toDate";
	
	public static final String PROP_NUM_LINESAMPLES = "numLineSamples";
	public static final String PROP_NUM_PAGESAMPLES = "numPageSamples";
	public static final String PROP_OPTION_PAGESAMPLES = "optionPageSamples";

	public static final String PROP_GPU_DEVICE_ENV_VAR = "GPU_DEVICE";

	public static final String PROP_SKIP_PAGES_WITH_MISSING_STATUS = "skipPagesWithMissingStatus";
	public static final String PROP_HAS_IMAGES = "hasImages";	
	public static final String PROP_KEEP_TEXT = "keepText";
	public static final String PROP_MAX_LINES_PER_PAGE = "maxLinesPerPage";
	
	public static final String PROP_ACCOUNTING_MODEL = "accountingModel";
	public static final String PROP_ACCOUNTING_MODEL_CREDITS_ON_COMPLETION = "creditsOnCompletion";
	
	public static final String PROP_ACCOUNTING_UNIT = "accountingUnit";
	public static final String PROP_ACCOUNTING_UNIT_PAGES = "PAGES";
	public static final String PROP_ACCOUNTING_UNIT_CHARS = "CHARS";
	
	// for struct recognition / P2PaLA:
	public static final String PROP_REGIONS = "regions";
	public static final String PROP_MERGED_REGIONS = "merge_regions";
	public static final String PROP_OUT_MODE = "out_mode";
	public static final String PROP_SPLIT_FRACTIONS = "splitFractions";
	public static final String PROP_MIN_AREA_PAR = "--min_area";
	public static final String PROP_RECTIFY_REGIONS_PAR = "--rectify_regions";
	public static final String PROP_ENRICH_EXISTING_TRANSCRIPTIONS_PAR = "enrichExistingTranscriptions";
	public static final String PROP_LABEL_REGIONS_PAR = "labelRegions";
	public static final String PROP_LABEL_LINES_PAR = "labelLines";
	public static final String PROP_LABEL_WORDS_PAR = "labelWords";
	public static final String PROP_KEEP_EXISTING_REGIONS = "keepExistingRegions";
	
	public static final String PROP_PARS_PREFIX = "pars."; // a default prefix for additional parameters to a certain job -> can be used to filter certain parameters
	public static final String PROP_IS_NEXT_GEN = "isNextGen";
    public static final String PROP_T2I_PRESERVER_LINE_ORDER = "preserveLineOrder";
	public static final String PROP_T2I_KEEP_UNMATCHED_LINES = "keepUnmatchedLines";
	public static final String PROP_T2I_USE_SOURCE_LINE_FEEDS = "useSourceLineFeeds";
	public static final String PROP_T2I_ADD_NOT_MATCHED_TEXT_IN_LAST_LINE = "addNotMatchedTextInLastLine";
	public static final String PROP_T2I_USE_CURRENT_TRANSKRIPT = "useCurrentTranskript";
	public static final String PROP_T2I_REDUCTION_METHOD = "reductionMethod";
	public static final String PROP_T2I_BLOCK_THRESHOLD = "blockThresh";
	public static final String PROP_T2I_LINE_THRESHOLD = "lineThresh";
	public static final String PROP_T2I_ALLOW_DOUBLE_MATCHING = "allowDoubleMatching";
    public static final String TEXT2IMAGE_MATCHING =  "Text2ImageMatching";
	public static final String PROP_S3 = "s3";
    public static final String PROP_S3_KEY = "s3Key";
    public static final String PROP_UPLOAD_DIR = "uploadDir";
}
