package eu.transkribus.core.exceptions;

import java.io.IOException;

public class FileSizeExceededException extends IOException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8784919566897404374L;
	
	public FileSizeExceededException() {
		super();
	}

	public FileSizeExceededException(String msg) {
		super(msg);
	}

}
