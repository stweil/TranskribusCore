package eu.transkribus.core.exceptions;

public class LayoutAnalysisFailedException extends RuntimeException {
	
	private static final long serialVersionUID = 4208506193523292679L;

	public LayoutAnalysisFailedException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public String getScriptOutput() {
		if (getCause() instanceof ScriptFailedException) {
			return ((ScriptFailedException) getCause()).getOutput();
		}
		else {
			return null;
		}
	}

}
