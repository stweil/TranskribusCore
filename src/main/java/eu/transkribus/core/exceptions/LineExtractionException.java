package eu.transkribus.core.exceptions;

import java.io.IOException;

public class LineExtractionException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8114305586149995511L;

	public LineExtractionException() {
		super();
	}
	
	public LineExtractionException(String message) {
		super(message);
	}
	
	public LineExtractionException(Throwable cause) {
		super(cause);
	}
	
	public LineExtractionException(String message, Throwable cause) {
		super(message, cause);
	}
}
