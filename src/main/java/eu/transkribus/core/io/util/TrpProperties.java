package eu.transkribus.core.io.util;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.exceptions.ParsePropertiesException;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.GsonUtil;

/**
 * Utility class containing methods for reading property files and mapping values to various data types<br/>
 */
public class TrpProperties {
		private static final Logger logger = LoggerFactory.getLogger(TrpProperties.class);
		
		protected String filename;
		protected String string;
		
		protected Properties props;
		
		public static TrpProperties fromString(String str) {
			return new TrpProperties(str, false);
		}		
		
		public static TrpProperties fromFile(String filename) {
			return new TrpProperties(filename, true);
		}
		
		public static TrpProperties fromProperties(Properties props) {
			return new TrpProperties(props);
		}
		
		public TrpProperties() {
//			props = new Properties();
			props  = new Properties() {
			    @Override
			    public synchronized Enumeration<Object> keys() {
			        return Collections.enumeration(new TreeSet<Object>(super.keySet()));
			    }
			};
		}
		
		public TrpProperties(Properties props) {
			this.props = CoreUtils.copyProperties(props);
		}
		
		public TrpProperties(String filename) throws ParsePropertiesException {
			this(filename, true);
		}
		
		public TrpProperties(String filenameOrString, boolean isFilename) throws ParsePropertiesException {
			if (isFilename) {
				this.filename = filenameOrString;
				loadPropsFromFilename();
			} else {
				this.string = filenameOrString;
				loadPropsFromString();
			}
		}
				
		public Object get(Object key) {
			return props.get(key);
		}
		
		public String getProperty(String key) {
			return props.getProperty(key);
		}
		
		public String getString(String name){
			return props.getProperty(name);
		}
				
		public Pattern getPattern(String name){
			return Pattern.compile(props.getProperty(name));
		}
		
		public String getOrDefault(String key, String defaultValue) {
			return (String) props.getOrDefault(key, defaultValue);
		}
		
		public boolean getOrDefault(String string, boolean defaultValue) {
			String v = (String) props.get(string);
			
			try {
				return v == null ? defaultValue : Boolean.valueOf(v);
			} catch (Exception e) {
				return defaultValue;
			}
		}
		
		public int getOrDefault(String string, int defaultValue) {
			String v = (String) props.get(string);
			try {
				return v == null ? defaultValue : Integer.valueOf(v);
			} catch (Exception e) {
				return defaultValue;
			}
		}
		
		public long getOrDefault(String string, long defaultValue) {
			String v = (String) props.get(string);
			
			try {
				return v == null ? defaultValue : Long.valueOf(v);
			} catch (Exception e) {
				return defaultValue;
			}
		}
		
		public double getOrDefault(String string, double defaultValue) {
			String v = (String) props.get(string);
			
			try {
				return v == null ? defaultValue : Double.valueOf(v);
			} catch (Exception e) {
				return defaultValue;
			}
		}		
		
		/**
		 * Duplicate of getIntProperty()
		 * 
		 * @param name
		 * @return
		 */
		@Deprecated 
		public Integer getInt(String name) {
			final String prop = props.getProperty(name);
			Integer value = null;
			if(prop != null){
				try{
					value = Integer.parseInt(props.getProperty(name));
				} catch (NumberFormatException nfe){
					nfe.printStackTrace();
				}
			}
			return value;
		}
		
		/**
		 * Duplicate of getBoolProperty()
		 * @param name
		 * @return
		 */
		@Deprecated
		public boolean getBool(String name){
			final String value = props.getProperty(name);
			boolean bool = false;
			if(value.equals("1") || value.equalsIgnoreCase("true")){
				bool = true;
			}
			return bool;
		}
				
		public Integer getIntProperty(String key) {
			String propStr = getProperty(key);
			if(propStr == null) {
				return null;
			}
			Integer retVal = null;
			try {
				retVal = Integer.parseInt(propStr);
			} catch (NumberFormatException nfe) {}
			return retVal;
		}
		
		public Double getDoubleProperty(String key) {
			String propStr = getProperty(key);
			if(propStr == null) {
				return null;
			}
			Double retVal = null;
			try {
				retVal = Double.parseDouble(propStr);
			} catch (NumberFormatException nfe) {}
			return retVal;
		}
		
		public Date getDateProperty(String key) {
			String dateStr = getString(key);
			if(dateStr == null) {
				return null;
			}
			Date date;
			try {
				date = CoreUtils.newDateFormat().parse(dateStr);
			} catch (ParseException e) {
				logger.error("Could not parse date String: {}", dateStr);
				date = null;
			}
			return date;
		}
		
		/**
		 * Get a Boolean value from the Properties.<br>
		 * Evaluation result is based on {@link Boolean#parseBoolean(String)}. Additionally, "1" will be evaluated as "true".<br>
		 * A missing (null) value will be interpreted as false!  
		 * 
		 * @param key
		 * @return
		 */
		public Boolean getBoolProperty(String key) {
			final String propStr = getProperty(key);
			if("1".equals(propStr)) {
				return true;
			}
			return Boolean.parseBoolean(propStr);
		}
		
		public List<String> getCsvStringListProperty(String key, boolean trimEntries) {
			return CoreUtils.parseStringList(getProperty(key), ",", trimEntries,  true);
		}
		
		public List<Integer> getCsvIntListProperty(String key) {
			return CoreUtils.parseIntList(getProperty(key));
			
//			List<Integer> result = new LinkedList<>();
//			String str = getProperty(key);
//			if(str != null && !str.isEmpty()) {
//				String[] arr = str.split(",");
//				for(String s : arr) {
//					result.add(Integer.parseInt(s));
//				}
//			}
//			return result;
		}

		protected Properties loadPropsFromFilename() throws ParsePropertiesException {
			try {
				props = CoreUtils.loadProperties(filename);
				return props;
			} catch (IOException e) {
				throw new ParsePropertiesException(e);
			}
		}
		
		protected Properties loadPropsFromString() throws ParsePropertiesException {
			try {
				props = CoreUtils.readPropertiesFromString(string);
				return props;
			} catch (IOException e) {
				throw new ParsePropertiesException(e);
			}
		}

		public Properties getProperties() {
			return props;
		}

		/**
		 * @deprecated Gson should not be used anymore in favor of Jackson
		 */
		public <T> T getJsonBean(String key, Class<T> clazz) {
			return GsonUtil.fromJson2(props.getProperty(key), clazz);
		}

		public String writeToString() {
			return CoreUtils.propertiesToString(props);
		}
		
		/**
		 * Sets a String value to the properties (see {@link Properties#setProperty(String, String)}).
		 * 
		 * If key or value is null, then nothing will be added and null is returned.
		 *  
		 * @param key
		 * @param value
		 * @return
		 */
		public Object setProperty(String key, String value) {
			if(key == null) {
				return null;
			}
			if(value == null) {
				logger.warn("Not inserting null value for key: " + key);
				return null;
			}
			return props.setProperty(key, value);
		}
		
		/**
		 * Sets a Integer value to the properties (see {@link Properties#setProperty(String, String)}).
		 * 
		 * If key or value is null, then nothing will be added and null is returned.
		 *  
		 * @param key
		 * @param value
		 * @return
		 */
		public Object setProperty(String key, Integer value) {
			if(value == null) {
				logger.warn("Not inserting (Integer)null value for key: " + key);
				return null;
			}
			return setProperty(key, ""+value);
		}
		
		public Object setProperty(String key, boolean value) {
			return setProperty(key, ""+value);
		}

		public Object remove(String key) {
			return props.remove(key);	
		}

		public boolean containsKey(String key) {
			return props.containsKey(key);
		}

		public boolean isEmpty() {
			return props.isEmpty();
		}

		/**
		 * first tries to parse the value of the given key as a JSON list, if that fails, as a 'regular' comma or(!) space separated list
		 */
		public List<String> getStrList(String key) {
			String val = getProperty(key);
			if (val==null) {
				return null;
			}
			
			try {
				return GsonUtil.toStrList(val);
			} catch (Exception e) {
				return CoreUtils.parseStringListOnCommasAndSpaces(val);
			}
		}
		
		/**
		 * tries to parse the value of the given key as map
		 */
		public Map<String, Object> getMapOrDefault(String key, Map<String, Object> defaultMap) {
			logger.debug("value of key: "+key);
			String val = getProperty(key);
			if (val==null) {
				logger.debug("value of map is null ");
				return defaultMap;
			}
			
			try {
				logger.debug("value of map: "+val);
				
				String[] tmp = val.split(":");
				if (tmp.length>1) {
					logger.debug("here is the key; " + tmp[0]);
					logger.debug("here is the value; " + tmp[1]);
					defaultMap.put(tmp[0], "\\"+ tmp[1]);
				}
				else {
					defaultMap.put("abbrev", "$");
				}
				
				//create json string
				if (!val.contains("{")) {
					
					val = val.replace("\n", ",");
					val = val.substring(0, val.lastIndexOf(","));
					val = "{" + val.concat("}");
					logger.debug("json value: " + val);
				}
				
				//return  defaultMap;
				
				//p.setProperty(JobConst.PROP_CUSTOM_TAG_MAP, CoreUtils.mapToString(getCustomTagParams().tagMap));

				return GsonUtil.fromJson(val, GsonUtil.MAP_TYPE);
				
				//return GsonUtil.toMapWithStringValues(val);
			} catch (Exception e) {
				//for testing 
				logger.debug("error getting param map of symbol - custom tag mapping");
				e.printStackTrace();
				defaultMap.put("abbrev", "$");
				return defaultMap;
			}
		}
		
		/**
		 * tries to parse the value of the given key as map
		 */
		public Map<String, Object> getMapOrDefault2(String key, Map<String, Object> defaultMap) {
			logger.debug("value of key: "+key);
			String val = getProperty(key);
			if (val==null) {
				logger.debug("value of map is null ");
				return defaultMap;
			}
			
			try {
				logger.debug("value of map: "+val);
				
				String delimsNewline = "[\n]+";
				String[] propsArray  = val.split(delimsNewline);
				
				for (String prop : propsArray) {
					
					String propLabel = prop.substring(0, prop.indexOf(":"));
					String propValue = prop.substring(prop.indexOf(":")+1);
					
					defaultMap.put(propLabel, propValue);
					
//					HashMap<String, Object> att = CssSyntaxTag.parseCssAttributes(propValue);
//					
//					logger.debug("prop label: " + propLabel);
//					logger.debug("prop value: " + propValue);
//					
//					for (String attKey : att.keySet()) {
//						logger.debug("attribute name: " + attKey);
//						logger.debug("attribute value: " + att.get(attKey));
//						
//					}
					
					
					
//					String delimsColon = "[:]+";
//					String[] singleProps  = prop.split(delimsColon);
//					for (String singleProp : singleProps) {
						
					
				}
				
				
				//val = CssSyntaxTag.unescapeCss(val);
					
//				val = val.replace(";}", "}");
//				val = val.replace("\n", ",");
//				val = val.replace("\\u003a", "");
//				val = val.replace("\\u003b", "");
//				val = val.replace("&", "");
//				
//				val = val.substring(0, val.lastIndexOf(","));
//				val = "{" + val.concat("}");
//				logger.debug("json value: " + val);
//				
//				
//				//return  defaultMap;
//				
//				//p.setProperty(JobConst.PROP_CUSTOM_TAG_MAP, CoreUtils.mapToString(getCustomTagParams().tagMap));
//
//				return GsonUtil.fromJson(val, GsonUtil.MAP_TYPE);
				
				//return GsonUtil.toMapWithStringValues(val);
			} catch (Exception e) {
				//for testing 
				logger.debug("error getting param map of symbol - custom tag mapping");
				e.printStackTrace();
				defaultMap.put("abbrev", "$");
				return defaultMap;
			}
			return defaultMap;
		}
		
		/**
		 * Sets the property as a JSON encoded list
		 */
		public void setStrList(String key, List<String> list) {
			setStrListAsJson(key, list);
		}
		
		public void setStrListAsJson(String key, List<String> list) {
			if (list==null) {
				remove(key);
			}
			else {
				setProperty(key, GsonUtil.toJson(list));
			}
		}
		
		public void setMapAsJson(Map<String, String> map) {
			if(map==null || map.isEmpty()) {
				return;
			}
			else {
				GsonUtil.toJson(map);
			}
		}
		
		public void setStrListAsCsv(String key, List<String> list) {
			if (list==null) {
				remove(key);
			}
			else {
				setProperty(key, CoreUtils.join(list));
			}
		}
		
		public List<String> getStrListOrDefault(String key, List<String> defaultList) {
			try {
				return getStrList(key);
			}
			catch (Exception e) {
				return defaultList;
			}
		}
		
//		public void parseAndSetString(String propertyName) throws IOException, IllegalAccessException, InvocationTargetException {
//			String value = props.getProperty(propertyName);
//			
//			logger.info(propertyName+" = "+value);
//			
//			if (StringUtils.isEmpty(value))
//				throw new IOException(propertyName+" must be provided!");
//			
//			BeanUtils.setProperty(this, propertyName, value);
//		}
}
