package eu.transkribus.core.io.util;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.exceptions.FileSizeExceededException;

// from: https://stackoverflow.com/questions/15445504/copy-inputstream-abort-operation-if-size-exceeds-limit/30072143#30072143
public class LimitedSizeInputStream extends InputStream {
	private static final Logger logger = LoggerFactory.getLogger(LimitedSizeInputStream.class);

    private final InputStream original;
    private final long maxSize;
    private long total;

    public LimitedSizeInputStream(InputStream original, long maxSize) {
        this.original = original;
        this.maxSize = maxSize;
    }

    @Override
    public int read() throws IOException {
        int i = original.read();
        if (i>=0) incrementCounter(1);
        return i;
    }

    @Override
    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte b[], int off, int len) throws IOException, FileSizeExceededException {
        int i = original.read(b, off, len);
        if (i>=0) incrementCounter(i);
        return i;
    }

    private void incrementCounter(int size) throws FileSizeExceededException {
        total += size;
        if (total>maxSize) throw new FileSizeExceededException("InputStream exceeded maximum size in bytes.");
    }

    @Override
    public void close() throws IOException {
    	logger.debug("Closing instance of {}", this.getClass().getCanonicalName());
    	original.close();
    	//InputStream::close in fact doesn't do anything in Java 8
    	super.close();
    }
}