package eu.transkribus.core.io.util;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.dea.fimagestore.core.util.MimeTypes;

public class ImgFileFilter implements FileFilter {
	private List<String> imgsToLoad = null;
	
	public ImgFileFilter(){}
	
	public ImgFileFilter(List<String> imgsToLoad){
		this.imgsToLoad = imgsToLoad;
	}

	@Override
	public boolean accept(File pathname) {
		final String mime = MimeTypes.getMimeType(FilenameUtils.getExtension(pathname.getName()));
		//is allowed mimetype and not starts with ".", which may occur on mac
		//FIXME pathname.isFile() does not have to be checked here?
		boolean isImgFile = pathname.isFile() && !pathname.getName().startsWith(".") && ImgPriority.containsMimeType(mime);
		return isImgFile && (this.imgsToLoad != null ? this.imgsToLoad.contains(pathname.getName()) : true);
//		return pathname.isFile() && !pathname.getName().startsWith(".") && ImgPriority.containsMimeType(mime);
	}
}
