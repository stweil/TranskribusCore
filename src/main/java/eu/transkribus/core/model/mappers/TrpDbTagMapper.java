package eu.transkribus.core.model.mappers;

import eu.transkribus.core.model.beans.TrpDbTag;
import eu.transkribus.core.model.beans.customtags.CssSyntaxTag;
import eu.transkribus.core.model.beans.searchresult.TagHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class TrpDbTagMapper {
    private static final Logger logger = LoggerFactory.getLogger(TrpDbTagMapper.class);

    /**
     * Map a {@link TagHit} to a legacy TrpDbTag object.
     * @param hit the TagHit to map
     * @param collId the collection ID context to add to the hit
     * @return a TrpDbTag object as required by the TX legacy tag search
     */
    public static TrpDbTag fromTagHit(TagHit hit, int collId) {
        TrpDbTag dbTag = new TrpDbTag();
        dbTag.setId(-1); // ID from DB not corresponding with ID from SOLR (int <-> string) -> never used in expert client anyway!
        dbTag.setDocid((int) hit.getDId());
        dbTag.setCollId(collId);
        dbTag.setPageid((int) hit.getPageId());
        dbTag.setPagenr((int) hit.getPageNr());
//	    	dbTag.setTs // FIXME -> not set in index?
        dbTag.setRegionid(hit.getLId());
        dbTag.setValue(hit.getValue());

        dbTag.setContextBefore(hit.getContextBefore());
        dbTag.setContextAfter(hit.getContextAfter());

        if (hit.getCustomAttributes() != null) {
            logger.trace("custom attributes = "+hit.getCustomAttributes());
            try {
                if (hit.getCustomAttributes().containsKey("offset")) {
                    dbTag.setOffset(Integer.valueOf(hit.getCustomAttributes().get("offset")));
                }
                if (hit.getCustomAttributes().containsKey("length")) {
                    dbTag.setLength(Integer.valueOf(hit.getCustomAttributes().get("length")));
                }
            } catch (Exception e) {
                logger.error("Could not set offset/length values: "+e.getMessage(), e);
            }

            HashMap<String, Object> attributes1 = new HashMap<>();
            for (String key : hit.getCustomAttributes().keySet()) {
                attributes1.put(key, hit.getCustomAttributes().get(key));
            }
            CssSyntaxTag ct = new CssSyntaxTag(hit.getType(), attributes1);
            dbTag.setCustomTagCss(ct.getCssString());
            logger.trace("css-str: "+ct.getCssString());
        }
        return dbTag;
    }
}
