package eu.transkribus.core.model.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import eu.transkribus.core.util.CoreUtils;

@Entity
@Table(name = "JOB_IMPL_REGISTRY")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpJobImplRegistry {
	@Id
	@Column(name="job_impl_registry_id")
	private int jobImplRegistryId;
	
	@Column(name="job_impl")
	private String jobImpl;
	
	@Column(name="job_tasks")
	private String jobTasks;
	
	@Column(name="job_type")
	private String jobType;
	
	@Column(name="free_for_all")
	private boolean freeForAll;
	
	@Transient
	private TrpCreditCosts costs;
	
	public TrpJobImplRegistry() {
	}

	public TrpJobImplRegistry(int jobImplRegistryId, String jobImpl, String jobTasks, String jobType) {
		this();
		this.jobImplRegistryId = jobImplRegistryId;
		this.jobImpl = jobImpl;
		this.jobTasks = jobTasks;
		this.jobType = jobType;
	}

	public int getJobImplRegistryId() {
		return jobImplRegistryId;
	}

	public void setJobImplRegistryId(int jobImplRegistryId) {
		this.jobImplRegistryId = jobImplRegistryId;
	}

	public String getJobImpl() {
		return jobImpl;
	}

	public void setJobImpl(String jobImpl) {
		this.jobImpl = jobImpl;
	}

	public String getJobTasks() {
		return jobTasks;
	}

	public void setJobTasks(String jobTasks) {
		this.jobTasks = jobTasks;
	}
	
	public List<String> getJobTasksList() {
		return CoreUtils.parseStringList(jobTasks, true);
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
	public TrpCreditCosts getCosts() {
		return costs;
	}

	public void setCosts(TrpCreditCosts costs) {
		this.costs = costs;
	}

	public boolean isFreeForAll() {
		return freeForAll;
	}

	public void setFreeForAll(boolean freeForAll) {
		this.freeForAll = freeForAll;
	}

	@Override
	public String toString() {
		return "TrpJobImplRegistry [jobImplRegistryId=" + jobImplRegistryId + ", jobImpl=" + jobImpl + ", jobTasks="
				+ jobTasks + ", jobType=" + jobType + ", freeForAll=" + freeForAll + ", costs=" + costs + "]";
	}
}
