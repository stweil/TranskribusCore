package eu.transkribus.core.model.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.rest.JobConst;
import io.swagger.v3.oas.annotations.Hidden;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PyLaiaHtrTrainConfig extends HtrTrainConfig implements Serializable {
	private static final long serialVersionUID = -7105887429420356434L;
	
	@Hidden
	public static final String NUM_EPOCHS_KEY = "Nr. of Epochs";
	@Hidden
	public static final String LEARNING_RATE_KEY ="Learning Rate";
	@Hidden
	public static final String BATCH_SIZE_KEY = "Train Size per Epoch";
	@Hidden
	public static final String EARLY_STOPPING_KEY = "Early Stopping";
	
	@Hidden
	public static final String TEXT_FEATS_CFG_KEY = "textFeatsCfg";
	@Hidden
	public static final String TRP_PREPROC_PARS_KEY = "trpPreprocPars";
	@Hidden
	public static final String CREATE_MODEL_PARS_KEY = "createModelPars";
	@Hidden
	public static final String TRAIN_CTC_PARS = "trainCtcPars";
	
//	@Schema(description = "the number of epochs. A positive natural number.", required=true)
//	protected Integer numEpochs = DEFAULT_NUM_EPOCHS;
//	protected int earlyStopping=DEFAULT_EARLY_STOPPING;
//	protected double learningRate=DEFAULT_LEARNING_RATE;
//	protected int batchSize=DEFAULT_BATCH_SIZE;
	
	protected TextFeatsCfg textFeatsCfg;
	protected TrpPreprocPars trpPreprocPars;
	protected PyLaiaCreateModelPars createModelPars;
	protected PyLaiaTrainCtcPars trainCtcPars;
	
	public PyLaiaHtrTrainConfig() {
		super();
		createModelPars = PyLaiaCreateModelPars.getDefault();
		trainCtcPars = PyLaiaTrainCtcPars.getDefault();
	}

	public Integer getNumEpochs() {
		return trainCtcPars.getMaxEpochs();
	}

	public void setNumEpochs(Integer numEpochs) {
		trainCtcPars.setMaxEpochs(numEpochs);
	}

	public int getEarlyStopping() {
		return trainCtcPars.getMaxNondecreasingEpochs();
	}

	public void setEarlyStopping(int earlyStopping) {
		trainCtcPars.setMaxNondecreasingEpochs(earlyStopping);
	}

	public Double getLearningRate() {
		return trainCtcPars.getLearningRate();
	}

	public void setLearningRate(Double learningRate) {
		trainCtcPars.setLearningRate(learningRate);
	}

	public int getBatchSize() {
		return trainCtcPars.getBatchSize();
	}

	public void setBatchSize(int batchSize) {
		trainCtcPars.setBatchSize(batchSize);
	}
	
	public TextFeatsCfg getTextFeatsCfg() {
		return textFeatsCfg;
	}

	public void setTextFeatsCfg(TextFeatsCfg textFeatsCfg) {
		this.textFeatsCfg = textFeatsCfg;
		this.trpPreprocPars = null;
	}
	
	public TrpPreprocPars getTrpPreprocPars() {
		return trpPreprocPars;
	}

	public void setTrpPreprocPars(TrpPreprocPars trpPreprocPars) {
		this.trpPreprocPars = trpPreprocPars;
		this.textFeatsCfg = null;
	}
	
	public boolean hasPreprocPars() {
		return getTrpPreprocPars()!=null || getTextFeatsCfg()!=null;
	}
	
	public void setDefaultPreprocPars() {
		this.trpPreprocPars = new TrpPreprocPars();
		this.textFeatsCfg = null;
	}

	public PyLaiaCreateModelPars getCreateModelPars() {
		return createModelPars;
	}

	public void setCreateModelPars(PyLaiaCreateModelPars createModelPars) {
		this.createModelPars = createModelPars;
	}

	public PyLaiaTrainCtcPars getTrainCtcPars() {
		return trainCtcPars;
	}

	public void setTrainCtcPars(PyLaiaTrainCtcPars trainCtcPars) {
		this.trainCtcPars = trainCtcPars;
	}
	
	@Override
	@Hidden
	public TrpProperties getParamProps() {
		TrpProperties p = super.getParamProps();
		if (textFeatsCfg != null) {
			p.getProperties().put(TEXT_FEATS_CFG_KEY, textFeatsCfg.toConfigString().replaceAll("\n", " "));
		}
		if (trpPreprocPars != null) {
			p.getProperties().put(TRP_PREPROC_PARS_KEY, trpPreprocPars.toJson().replaceAll("\n", " "));
		}
		if (createModelPars != null) { 
			p.getProperties().put(CREATE_MODEL_PARS_KEY, createModelPars.toSingleLineString());
		}
		if (trainCtcPars != null) { 
			p.getProperties().put(TRAIN_CTC_PARS, trainCtcPars.toSingleLineString());
		}
		
		return p;
	}

	@Override
	public String toString() {
		return "PyLaiaHtrTrainConfig [textFeatsCfg=" + textFeatsCfg + ", trpPreprocPars=" + trpPreprocPars
				+ ", createModelPars=" + createModelPars + ", trainCtcPars=" + trainCtcPars + ", getModelMetadata()="
				+ getModelMetadata() + ", getColId()=" + getColId() + ", getCustomParams()=" + getCustomParams()
				+ ", getImgType()=" + getImgType() + ", getTrain()=" + getTrain() + ", getTest()=" + getTest()
				+ ", getTrainGt()=" + getTrainGt() + ", getTestGt()=" + getTestGt() + ", getReverseTextParams()="
				+ getReverseTextParams() + ", getCustomTagParams()=" + getCustomTagParams() + ", getTagsToOmit()="
				+ getTagsToOmit() + ", hasTagsToOmit()=" + hasTagsToOmit() + ", isTestAndTrainOverlapping()="
				+ isTestAndTrainOverlapping()+", isUseExistingLinePolygons() = " + isUseExistingLinePolygons() + "]";
	}

	@Override
	public String getType() {
		return TrpHtr.TYPE_TEXT;
	}
}
