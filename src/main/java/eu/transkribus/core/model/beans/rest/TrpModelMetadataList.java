package eu.transkribus.core.model.beans.rest;

import java.sql.Timestamp;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.searchresult.FacetRepresentation;

@XmlRootElement //(name="trpHtrList")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpModelMetadata.class, Timestamp.class, FacetRepresentation.class})
public class TrpModelMetadataList extends JaxbPaginatedList<TrpModelMetadata> {
	private List<FacetRepresentation> facets;
	public TrpModelMetadataList() {}
	public List<FacetRepresentation> getFacets() {
		return facets;
	}
	public void setFacets(List<FacetRepresentation> facets) {
		this.facets = facets;
	}
}
