package eu.transkribus.core.model.beans.pagecontent_trp;

import java.awt.Rectangle;
import java.util.Comparator;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.pagecontent.RegionType;
import eu.transkribus.core.model.beans.pagecontent.TextLineType;
import eu.transkribus.core.model.beans.pagecontent.WordType;
import eu.transkribus.core.util.IntRange;
import eu.transkribus.core.util.PointStrUtils;

/**
 * Updated version (should) sort *lines*: if lines overlaps in Y direction -> sort XY -> otherwise sort YX
 *  
 */
public class TrpElementCoordinatesComparator_XY_WhenYOverlaps<T> implements Comparator<T> {
	private final static Logger logger = LoggerFactory.getLogger(TrpElementCoordinatesComparator_XY_WhenYOverlaps.class);
	
	boolean ltr = true;

	public TrpElementCoordinatesComparator_XY_WhenYOverlaps(boolean ltr) {
		this.ltr = ltr;
	}
	
	
	private boolean isRegionLineOrWord(T o) {
		return (o instanceof RegionType || TextLineType.class.isAssignableFrom(o.getClass()) || WordType.class.isAssignableFrom(o.getClass()));		
	}

	@Override
	public int compare(T o1, T o2) {
//		if (!isRegionLineOrWord(o1) || !isRegionLineOrWord(o2))
//			return 0;
		
		logger.trace("compare in TrpElementCoordinatesComparator4Columns");
		
		String coords1="", coords2="";
					
//			if (o1 instanceof PrintSpaceType) {
//				coords1 = ((TrpPrintSpaceType) o1).getCoords().getPoints();
//				coords2 = ((TrpPrintSpaceType) o2).getCoords().getPoints();
//			}		
		
		boolean performXYIfYOverlaps = true;
		boolean sortByXY=false;
		
		if (o1 instanceof RegionType) {
			RegionType r1 = (RegionType) o1;
			RegionType r2 = (RegionType) o2;
//				System.out.println("region1 id: " + r1.getId());
//				System.out.println("region2 id: " + r2.getId());
			if (r1.getCoords() != null && r2.getCoords() != null) {
				coords1 = r1.getCoords().getPoints();
				coords2 = r2.getCoords().getPoints();					
			}
		}
		else if (TextLineType.class.isAssignableFrom(o1.getClass())) {
			// if existing, take baseline to compare position of lines
			if (((TextLineType) o1).getBaseline() != null && ((TextLineType) o2).getBaseline() != null){
				coords1 = ((TextLineType) o1).getBaseline().getPoints();
				coords2 = ((TextLineType) o2).getBaseline().getPoints();
			} else { //fall back if there are no baselines
				coords1 = ((TextLineType) o1).getCoords().getPoints();
				coords2 = ((TextLineType) o2).getCoords().getPoints();					
			}
//			coords1 = ((TextLineType) o1).getCoords().getPoints();
//			coords2 = ((TextLineType) o2).getCoords().getPoints();
		}
		else if (o1 instanceof TrpBaselineType) {
			coords1 = ((TrpBaselineType) o1).getPoints();
			coords2 = ((TrpBaselineType) o2).getPoints();
		}
		else if (WordType.class.isAssignableFrom(o1.getClass())) {
			sortByXY=true;
			performXYIfYOverlaps=false;
			
			WordType w1 = (WordType) o1;
			WordType w2 = (WordType) o2;
			
			if (w1.getCoords()!=null && w2.getCoords()!=null) {
				coords1 = w1.getCoords().getPoints();
				coords2 = w2.getCoords().getPoints();					
			}
		}
		
//			if (coords1.isEmpty() || coords2.isEmpty()) {
//				throw new Exception("No coordinates in one of the objects - should not happen!");
//			}
		
//		Pair<String, String> fixedCoords = TrpShapeTypeUtils.invertCoordsCommonRegionOrientation(coords1, coords2, o1, o2);
//		coords1 = fixedCoords.getLeft();
//		coords2 = fixedCoords.getRight();		
		
		Rectangle b1 = PointStrUtils.getBoundingBox(coords1);
		Rectangle b2 = PointStrUtils.getBoundingBox(coords2);
//			Point pt1 = new Point(b1.x, b1.y);
//			Point pt2 = new Point(b2.x, b2.y);	
		
//		performColumnSensitiveComparison = true; // for testing purposes to force column sensitive comparison on all elements
		
		if (performXYIfYOverlaps) {
			return compareByYXIfOverlappingOnYAxis(b1, b2);
		}
		else if (sortByXY) {
			return compareByXY(b1.x, b2.x, b1.y, b2.y);
		}
		else {
			return compareByYX(b1.x, b2.x, b1.y, b2.y);
		}
			
	}
	
	/** First compare by y, then x */
	private int compareByYX(int x1, int x2, int y1, int y2) {
//		int yCompare = Integer.compare(y1, y2);
//		System.out.println("yCompare " +yCompare);
//		return (yCompare != 0) ? yCompare : compareByXY(x1,x2,y1,y2);
		
		int yCompare = Integer.compare(y1, y2);
		return (yCompare != 0) ? yCompare : Integer.compare(x1, x2);
	}
	
	//neuer Ansatz
	private int compareByYXIfOverlappingOnYAxis(Rectangle b1, Rectangle b2) {
		double xFrac = 0.8d;
		double yFrac = 1.0d;
		
		int ox = IntRange.getOverlapLength(b1.x, b1.width, b2.x, b2.width);
		int oy = IntRange.getOverlapLength(b1.y, b1.height, b2.y, b2.height);
		
		double b1CenterY = b1.getCenterY();
		double b2CenterY = b2.getCenterY();
		
		double b1CenterX = b1.getCenterX();
		double b2CenterX = b2.getCenterX();
		
		double b1StartX = b1.getMinX();
		double b2StartX = b2.getMinX();
		
		logger.debug("compare: mid1y " + b1CenterY);
		logger.debug("compare: b2.y+b2.height/2 " + Math.addExact(b2.y, b2.height));
		logger.debug("compare: b2.y " + b2.y);
		
		logger.debug("compare: mid2y " + b2CenterY);
		logger.debug("compare: b1.y+b1.height/2 " + Math.addExact(b1.y, b1.height));
		logger.debug("compare: b1.y " + b1.y);
		
		double mid1x = Math.addExact(b1.x,b1.width)/2.0;
		double mid2x = (b2.x + b2.width)/2;
		
		logger.trace("b1.width = "+b1.width+", b2.width = "+b2.width);
		int minOverlap = (int) (Math.min(b1.width, b2.width)*xFrac); // fraction of the width of the smaller rectangle
		int minOverlapY = (int) (Math.min(b1.height, b2.height)*yFrac); // fraction of the height of the smaller rectangle
		logger.debug("minOverlapY = "+minOverlapY+" oy = "+oy);
		
		logger.trace("overlap = "+ox+", minOverlap = "+minOverlap);
		if ( (b1.contains(b1CenterX, b2CenterY)) || b2.contains(b2CenterX, b1CenterY)) { //check if shapes are on the same height
			logger.debug("xy compare");
			if (ltr) {
				return Integer.compare(b1.x, b2.x);
			}
			else {
				return Integer.compare(b1.x, b2.x)*-1;
			}
			
		}
		else {
			logger.debug("yx compare");
			int yx = compareByYX(b1.x, b2.x, b1.y, b2.y);
			return yx;

		}
	}
	
	private int compareByYXIfOverlappingOnYAxisOldButworking(Rectangle b1, Rectangle b2) {
		double xFrac = 0.8d;
		double yFrac = 1.0d;
		
		int ox = IntRange.getOverlapLength(b1.x, b1.width, b2.x, b2.width);
		int oy = IntRange.getOverlapLength(b1.y, b1.height, b2.y, b2.height);
		
		logger.trace("b1.width = "+b1.width+", b2.width = "+b2.width);
		int minOverlap = (int) (Math.min(b1.width, b2.width)*xFrac); // fraction of the width of the smaller rectangle
		int minOverlapY = (int) (Math.min(b1.height, b2.height)*yFrac); // fraction of the height of the smaller rectangle
		logger.debug("minOverlapY = "+minOverlapY+" oy = "+oy);
		
		logger.trace("overlap = "+ox+", minOverlap = "+minOverlap);
		if (oy>=-10 && oy <= minOverlapY) { // sort by xy if overlapping in y-direction
		//if (Math.abs(oy) <= minOverlapY) { // sort by xy if overlapping in y-direction
			logger.trace("xy compare");
			if (ltr) {
				return Integer.compare(b1.x, b2.x);
			}
			else {
				return Integer.compare(b1.x, b2.x)*-1;
			}
			
		}
		else {
			logger.trace("yx compare");
			int yx = compareByYX(b1.x, b2.x, b1.y, b2.y);
			return yx;

		}
	}
	
	/** First compare by x, then y */
	private int compareByXY(int x1, int x2, int y1, int y2) {
		int xCompare = Integer.compare(x1, x2);
		return (xCompare != 0) ? xCompare : Integer.compare(y1, y2);
	}	

}
