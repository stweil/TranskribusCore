package eu.transkribus.core.model.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Type to be used in a list as member of other objects.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpEntityAttribute implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "ID")
	private int attributeId;
	
	@Column 
	private int entityId;
	@Column
	private String type;
	@Column
	private String name;
	@Column
	private String value;
	
	public TrpEntityAttribute() {}
	
	public TrpEntityAttribute(TrpEntityAttribute att) {
		attributeId = att.getAttributeId();
		entityId = att.getEntityId();
		type = att.getType();
		name = att.getName();
		value = att.getValue();
	}

	public int getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "TrpEntityAttribute [attributeId=" + attributeId + ", entityId=" + entityId + ", type=" + type
				+ ", name=" + name + ", value=" + value + "]";
	}
}
