package eu.transkribus.core.model.beans.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import eu.transkribus.core.model.beans.TrpCollection;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ TrpCollection.class })
public class TrpCollectionList extends JaxbPaginatedList<TrpCollection> {
	
	public TrpCollectionList() {
		super();
	}

	public TrpCollectionList(List<TrpCollection> list, int total, int index, int nValues, 
    		String sortColumnField, String sortDirection){
		super(list, total, index, nValues, sortColumnField, sortDirection);
	}
}
