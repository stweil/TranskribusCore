package eu.transkribus.core.model.beans.searchresult;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchReplaceParams {

    String replaceTerm;
    boolean isAnnotation = false;
    @XmlElementWrapper(name="pageHits")
    @XmlElement(name="PageHit")
    protected List<PageHit> pageHits;

    public SearchReplaceParams() {
        pageHits = new ArrayList<>();
    }

    public String getReplaceTerm() {
        return replaceTerm;
    }

	public void setReplaceTerm(String replaceTerm) {
        this.replaceTerm = replaceTerm;
    }
	
    public boolean isAnnotation() {
		return isAnnotation;
	}

	public void setAnnotation(boolean isAnnotation) {
		this.isAnnotation = isAnnotation;
	}

    public List<PageHit> getPageHits() {
        return pageHits;
    }

    public void setPageHits(List<PageHit> pageHits) {
        this.pageHits = pageHits;
    }

    @Override
	public String toString() {
		return "SearchReplaceParams [replaceTerm=" + replaceTerm + ", isAnnotation=" + isAnnotation + ", pageHits="
				+ pageHits + "]";
	}
}
