package eu.transkribus.core.model.beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.util.JacksonUtil;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LA2TrainConfig extends HtrTrainConfig implements Serializable {
	private static final Logger logger = LoggerFactory.getLogger(LA2TrainConfig.class);
    private static final long serialVersionUID = 8863676001947093656L;
	
	public static final int DEFAULT_NUM_EPOCHS = 20000;
	public static final double DEFAULT_LEARNING_RATE = 0.001d;
	public static final String DEFAULT_MODEL_TYPE = "instance_101";
    // public static final String AUTO_GENERATE_INPUT_DEF = "auto_generate_input_def";
    public static final String DEFAULT_INPUT_TYPE = "auto_generate_input_def";
	public static final String DEFAULT_INPUT_TYPE_JSON = "";
	
	@Hidden
	public final static String MODEL_TYPE_KEY = "Model Type";	
	@Hidden
	public final static String INPUT_TYPE_KEY = "Input Type";
	@Hidden
	public final static String INPUT_TYPE_JSON_KEY = "Input Type JSON";		    
	
	@Schema(description = "The number of epochs to be trained. A positive natural number.", required=true, defaultValue = ""+DEFAULT_NUM_EPOCHS)
	protected Integer numEpochs = DEFAULT_NUM_EPOCHS;
	@Schema(description = "The learning rate for traininig. Floating point in scientific notation, e.g. 2E-3", required=false, defaultValue = ""+DEFAULT_LEARNING_RATE)
	protected Double learningRate=DEFAULT_LEARNING_RATE;
	@Schema(description = "The model type - one of: 'instance_50', 'instance_101'", required=false, defaultValue = DEFAULT_MODEL_TYPE)
	protected String modelType = DEFAULT_MODEL_TYPE;
	@Schema(description = "A map with advanced parameter", required=false)
	protected ParameterMap advancedParams;
//	@Schema(description = "The structure types to train. No value is 'auto_generate'", required=false)
//	private TrpStructureStats structures;
	@Schema(description = "The type of input. Can be: 'text_region', 'table', 'auto_generate'", required=false)
    protected String inputType;
	@Schema(description = "A list of input definitions.", required=false)
	protected List<LA2InputType> inputTypeJson = new LinkedList<>();
	@Schema(description = "If true, the number of epochs is interpreted as the number of iterations. If false, the number of epochs is interpreted as the number of epochs.", required=false, defaultValue = "true")	
	protected boolean epochsIsIterations = true;

	public LA2TrainConfig() {
		super();
	}

	public void setInputTypeJsonList(List<LA2InputType> list) {
		inputTypeJson = list;
	}

	public List<LA2InputType> getInputTypeJsonList() {
		return inputTypeJson;
	}

	public Integer getNumEpochs() {
		return numEpochs==null ? DEFAULT_NUM_EPOCHS : numEpochs;
	}

	public void setNumEpochs(Integer numEpochs) {
		this.numEpochs = numEpochs;
	}

	public Double getLearningRate() {
		return learningRate==null ? DEFAULT_LEARNING_RATE : learningRate;
	}

	public void setLearningRate(Double learningRate) {
		this.learningRate = learningRate;
	}

	public String getModelType() {
		return modelType==null ? DEFAULT_MODEL_TYPE : modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInputType() {
        return inputType;
    }

//	public TrpStructureStats getStructures() {
//		return structures;
//	}
//	public void setStructures(TrpStructureStats structures) {
//		this.structures = structures;
//	}

	
	public ParameterMap getAdvancedParams() {
		return advancedParams;
	}

	public void setAdvancedParams(ParameterMap advancedParams) {
		this.advancedParams = advancedParams;
	}

	/**
	 * Returns a TrpProperties object with human readable property names for each config parameter
	 */
	public TrpProperties toParamProps() {
		TrpProperties p = new TrpProperties();
		p.setProperty(HtrTrainConfig.NUM_EPOCHS_KEY, numEpochs);
		p.setProperty(HtrTrainConfig.LEARNING_RATE_KEY, ""+learningRate);
		p.setProperty(LA2TrainConfig.MODEL_TYPE_KEY, modelType);
        p.setProperty(LA2TrainConfig.INPUT_TYPE_KEY, inputType);
		p.setProperty(LA2TrainConfig.INPUT_TYPE_JSON_KEY, buildInputTypeJsonStr());
		if (advancedParams!=null) {
			for (String key : advancedParams.getParamMap().keySet()) {
				p.setProperty(key, advancedParams.getParameterValue(key));
			}
		}	
		return p;
	}

	@Hidden
	private String buildInputTypeJsonStr() {
		try {
			return JacksonUtil.createDefaultMapper(false).writeValueAsString(inputTypeJson);
		} catch (JsonProcessingException e) {
			logger.error("Could not marshal inputTypeJson to String!", e);
			return null;
		}
	}

	@Override
	public String toString() {
		return "LA2TrainConfig [numEpochs=" + numEpochs + ", learningRate=" + learningRate + ", modelType=" + modelType + ", inputType=" + inputType + ", inputTypeJson=" +inputTypeJson + ", epochsIsIterations=" + epochsIsIterations
				+ (advancedParams==null ? "" : ", advancedParams=("+advancedParams.toSingleLineString()+")") + "]";			
	}

	@Override
	public String getType() {
		return TrpHtr.TYPE_LAYOUT;
	}

	public boolean isEpochsIsIterations() {
		return epochsIsIterations;
	}

	public void setEpochsIsIterations(boolean epochsIsIterations) {
		this.epochsIsIterations = epochsIsIterations;
	}
	
}
