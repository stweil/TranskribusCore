package eu.transkribus.core.model.beans.pylaia_kws;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrBlockType;
import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrLineType;
import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrPageType;
import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrType;
import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrWordType;
import eu.transkribus.core.util.GsonUtil;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PyLaiaKwsReader {
	private static final Logger logger = LoggerFactory.getLogger(PyLaiaKwsReader.class);
	public class PyLaiaKwsLine {
		public List<PyLaiaKwsWord> words;
		public List<PyLaiaKwsLineAlternative> alternatives;
		public String lid;
		public String rid;
		public String pn;
		
		public PyLaiaKwsLine() {
			
		}
	}
	
	public class PyLaiaKwsLineAlternative {
		public List<PyLaiaKwsWord> words = new ArrayList<>();
		
		public PyLaiaKwsLineAlternative() {
		}
		
		public String getLineTxt() {
			return words.stream().map(w -> w.txt).collect(Collectors.joining(" "));
		}
		
	}
	
	public class PyLaiaKwsWord {
		public String pn;
		public String lid;
		public String rid;
	    public List<Point> pts = new ArrayList<>();
	    public Polygon p;
	    public Rectangle bb;
	    public String txt;		
		
		public PyLaiaKwsWord(Map<String, ?> wordMap) {
			pn = (String) wordMap.get("pn");
			lid = (String) wordMap.get("lid");
			rid = (String) wordMap.get("rid");
			txt = (String) wordMap.get("txt");
			pts = new ArrayList<>();
			p = new Polygon();
			for (List<Double> ptL : (List<List<Double>>) wordMap.get("pts")) {
				pts.add(new Point(ptL.get(0).intValue(), ptL.get(1).intValue()));
				p.addPoint(ptL.get(0).intValue(), ptL.get(1).intValue());
			}
			bb = p.getBounds();
		}
	    
		@Override
		public String toString() {
			return "WordData [pn=" + pn + ", lid=" + lid + ", rid=" + rid + ", pts=" + pts + ", txt=" + txt + "]";
		}
	}
	
	Reader reader;
	Map<String, ?> map;
	
	public PyLaiaKwsReader(String path) throws IOException {
		reader = Files.newBufferedReader(Paths.get(path));
		map = GsonUtil.GSON.fromJson(reader, Map.class);
	}
	
	public Set<String> getLineIds() {
		return map.keySet();
	}
		
	public List<Map<String, ?>> getWordsOfLineAsMap(String lid) {
		return (List<Map<String, ?>>) getLineData(lid).get("words");
	}
	
	/**
	 * Returns a list of all words found for this line - *not* sorted!
	 */
	public List<PyLaiaKwsWord> getWordsOfLine(String lid, boolean sorted) {
		List<PyLaiaKwsWord> wd = new ArrayList<>();
		for (Map<String, ?> m : getWordsOfLineAsMap(lid)) {
			wd.add(new PyLaiaKwsWord(m));
		}
		if (sorted) {
			wd.sort((w1, w2) ->  Integer.compare(w1.bb.x, w2.bb.x) );
		}
		return wd;
	}
	
	/**
	 * Returns a list of lists with indices to the words of {@link #getWordsOfLine(String)} in this line alternative
	 */
	public List<List<Integer>> getLineAlternativeIndices(String lid) {
		// map Double's from JSON to Int's:
		return ((List<List<Double>>) getLineData(lid).get("lines")).stream().map(alt -> {
			return alt.stream().map(i -> i.intValue()).collect(Collectors.toList());
		}).collect(Collectors.toList());
	}	
	
	public List<PyLaiaKwsLineAlternative> getLineAlternatives(String lid) {
		List<PyLaiaKwsWord> words = getWordsOfLine(lid, false);
		// map Double's from JSON to Int's:
		return ((List<List<Double>>) getLineData(lid).get("lines")).stream().map(alt -> {
			PyLaiaKwsLineAlternative line = new PyLaiaKwsLineAlternative();
			alt.stream().forEach(i -> line.words.add(words.get(i.intValue())));
			return line;
		}).collect(Collectors.toList());
	}
	
	public String getRid(String lid) {
		return (String) getLineData(lid).get("rid");
	}
	
	public String getPn(String lid) {
		return (String) getLineData(lid).get("pn");
	}
	
	public List<PyLaiaKwsLine> getLines() {
		List<PyLaiaKwsLine> lines = new ArrayList<>();
		for (String lid : getLineIds()) {
			PyLaiaKwsLine line = new PyLaiaKwsLine();
			line.words = getWordsOfLine(lid, false);
			line.alternatives = getLineAlternatives(lid);
			line.lid = lid;
			line.rid = getRid(lid);
			line.pn = getPn(lid);
			lines.add(line);
		}
		return lines;
	}
	
	public Map<String, ?> getLineData(String lid) {
		return (Map<String, ?>) map.get(lid);
	}	
	
	public void createMiniOCR(int pageId, int pageWidth, int pageHeight, String outPath) throws JAXBException {
		Map<String, List<MiniOcrLineType>> rMap = new HashMap<>();
		String pn = "";
		for (PyLaiaKwsLine l : getLines()) {
			pn = l.pn;
			MiniOcrLineType mLine = new MiniOcrLineType();
			mLine.setLineId(l.lid);
			List<MiniOcrWordType> mWords = new ArrayList<>();
			
			for (int i=0; i<l.alternatives.size(); ++i) {
				PyLaiaKwsLineAlternative alt = l.alternatives.get(i);
				
				for (PyLaiaKwsWord w : alt.words) {
					MiniOcrWordType mW = new MiniOcrWordType();
					
					String wordText = w.txt != null ? w.txt : "";
					wordText = wordText.replaceAll("\\p{Punct}", ""); // remove all punctuation marks
					wordText = wordText.replaceAll("(?U)\\p{Punct}", ""); // remove Unicode punctuation marks as well, cf. https://stackoverflow.com/questions/73841877/regex-u-ppunct-is-missing-some-unicode-punctuation-signs-in-java
					wordText = wordText.replace("¬", "");
					wordText = wordText.replaceAll("\u00ad", ""); // remove all soft-hyphens
					wordText = wordText.replace("\u00a0",""); // remove non-breaking space
					wordText = wordText.replaceAll("\\p{C}|\\s", ""); // replace all whitespace and control characters
					wordText = wordText.replace("\u21FF", ""); // remove alternative word separator
					wordText = wordText.trim();
					
					if (wordText.isEmpty()) { // index does not accept empty words -> set to space at least...
						if (i==0) {
							wordText = "x";
						}
						else {
							continue; // ... and skip alternative words
						}
						// wordText = " ";
					}
					mW.setText(wordText); 
					mW.setCoords(w.bb.x, w.bb.y, w.bb.width, w.bb.height);
					
					if (i==0) {
						// for best alternative (i.e. i==0), just add word, including it's bounding box
						mWords.add(mW);
					}
					else { // FIXME parsing *all* words of all alternatives of n>0 is way too inefficient (too many equal words)
						// find word of best-line with most overlap and add text as alternative
						MiniOcrWordType wMax = null;
						int maxOverlap = -1;
						for (MiniOcrWordType mW1 : mWords) {
							int overlap = mW1.overlap(mW);
							if (overlap > maxOverlap) {
								maxOverlap = overlap;
								wMax = mW1;
							}
						}
						if (wMax != null) { // TODO (maybe) adapt bounding box of wMax to mW?
							if (!Arrays.asList(wMax.getTextContent().split("\u21FF")).contains(mW.getTextContent())) {
								wMax.setText(wMax.getTextContent()+'\u21FF'+mW.getTextContent());
							}
						}
					}
				}
			}
			mLine.setWords(mWords);
//			System.out.println("mWords "+mWords.size());
			
			// add line to region map:
			List<MiniOcrLineType> mLines = rMap.get(l.rid);
			if (mLines == null) {
				mLines = new ArrayList<>();
				rMap.put(l.rid, mLines);
			}
			mLines.add(mLine);
//			System.out.println("mLines.size() = "+mLines.size());
		}
		
		List<MiniOcrBlockType> ocrBlocks = new ArrayList<>();
		for (String rid : rMap.keySet()) {
			MiniOcrBlockType ocrBlock = new MiniOcrBlockType();
			ocrBlock.setRegionId(rid);
			ocrBlock.setLines(rMap.get(rid));
			ocrBlocks.add(ocrBlock);
		}

        MiniOcrPageType ocrPage = new MiniOcrPageType();
        ocrPage.setId(pageId);
        ocrPage.setDimensions(pageWidth, pageHeight);
        ocrPage.setBlocks(ocrBlocks);

        MiniOcrType miniOcrDoc = new MiniOcrType();
        miniOcrDoc.setPage(ocrPage);
        
        JAXBContext context = JAXBContext.newInstance(MiniOcrType.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        //mar.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

//        mar.marshal(miniOcrDoc, new File(basePath + File.separator + docId + File.separator + page.getPageNr() +".xml"));
        logger.debug("writing MiniOCR to: {}", outPath);
        mar.marshal(miniOcrDoc, new File(outPath));
	}

}
