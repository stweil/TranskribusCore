package eu.transkribus.core.model.beans.searchresult;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TagSearchResult {

    protected String params;
    protected long numResults;
    
    @XmlElementWrapper(name="tagHits")
	@XmlElement(name="tagHit")
    protected List<TagHit> tagHits;

    protected List<FacetRepresentation> facets;

    public TagSearchResult(){}

    
    public void setTagHits(List<TagHit> hits){
        this.tagHits = hits;
    }

    public List<TagHit> getTagHits(){
        return tagHits;
    }    
    
    public void setNumResults(long n){
        numResults = n;
    }
    
    public long getNumResults(){
        return numResults;
    }

    public void setFacets(List<FacetRepresentation> facets){
        this.facets = facets;
    }

    public List<FacetRepresentation> getFacets(){
        return facets;
    }

    public void setParams(String p){
        this.params = p;
    }

    public String getParams(){
        return params;
    }

    @Override
	public String toString() {
		return "TagSearchResult [params=" + params + ", numResults=" + numResults + ", n-tagHits=" + (tagHits!=null ? tagHits.size() : 0)
				+ ", facets=" + facets + "]";
	}
    
}
