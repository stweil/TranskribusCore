package eu.transkribus.core.model.beans;

import java.io.Serializable;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.job.enums.JobType;

public class LA2InferenceConfig implements Serializable {

    private final static Logger logger = LoggerFactory.getLogger(LA2InferenceConfig.class);

    public static final String APPROX_POLY_FRAC_KEY = "approxPolyFrac";
    public static final String ADD_TO_PAGE_XML_KEY = "addToPageXML";
    public static final String THRESHOLD_KEY = "threshold";
	public static final String ENRICH_EXISTING_TRANSCRIPTIONS_KEY = "enrichExistingTranscriptions";
	public static final String KEEP_EMPTY_REGIONS_KEY = "keepEmptyRegions";
	public static final String SPLIT_LINES_KEY = "splitLines";
	public static final String LINE_OVERLAP_FRACTION_KEY = "lineOverlapFraction";
	public static final String CLUSTER_LINES_WITHOUT_REGIONS_KEY = "clusterLinesWithoutRegions";

    public static final Double DEFAULT_APPROX_POLY_FRAC = .7;
    public static final boolean DEFAULT_ADD_TO_PAGE_XML = false;
    public static final Double DEFAULT_THRESHOLD = .75;
	public static final boolean DEFAULT_KEEP_EMPTY_REGIONS = false;
	public static final boolean DEFAULT_SPLIT_LINES = false;
	public static  final Double DEFAULT_LINE_OVERLAP_FRACTION = 0.05;
	public static final boolean DEFAULT_CLUSTER_LINES_WITHOUT_REGIONS = false;

    protected Double approxPolyFrac = DEFAULT_APPROX_POLY_FRAC;
	protected boolean addToPageXML = DEFAULT_ADD_TO_PAGE_XML;
    protected Double threshold = DEFAULT_THRESHOLD;
	protected boolean keepEmptyRegions = DEFAULT_KEEP_EMPTY_REGIONS;
	protected boolean splitLines = DEFAULT_SPLIT_LINES;
	protected Double lineOverlapFraction = DEFAULT_LINE_OVERLAP_FRACTION; // The minimum overlap fraction between a line and a region to split the line. Only applies if enrichExistingTranscriptions is set
	protected boolean clusterLinesWithoutRegions = DEFAULT_CLUSTER_LINES_WITHOUT_REGIONS;

    protected JobImpl jobImpl;

    protected Properties props;

    public LA2InferenceConfig() {
        props = new Properties();
    }

	public LA2InferenceConfig(final JobImpl jobImpl) {
		this(jobImpl, (Properties) null);
	}
	
	public LA2InferenceConfig(final JobImpl jobImpl, TrpProperties props) {
		this(jobImpl, props.getProperties());
	}
	
	public LA2InferenceConfig(final JobImpl jobImpl, Properties props) {
		this();
		if(jobImpl == null || !jobImpl.getTask().getJobType().equals(JobType.layoutAnalysis)) {
			throw new IllegalArgumentException("Not a layout analysis JobImpl!");
		}
		if(props == null) {
			props = new Properties();
		}
		this.jobImpl = jobImpl;
		this.props = props;
		this.initFieldsFromJobProps(props);
		logger.debug(toString());
	}


	protected void initFieldsFromJobProps(Properties props) {
		TrpProperties trpProps = TrpProperties.fromProperties(props);
		if (props.getProperty(APPROX_POLY_FRAC_KEY) != null) {
            setApproxPolyFrac(trpProps.getDoubleProperty(APPROX_POLY_FRAC_KEY));
		}
		if (props.getProperty(THRESHOLD_KEY) != null) {
            setThreshold(trpProps.getDoubleProperty(THRESHOLD_KEY));
		}
		if (props.getProperty(ADD_TO_PAGE_XML_KEY) != null) {
            setAddToPageXML(trpProps.getBoolProperty(ADD_TO_PAGE_XML_KEY));
		}
		if (props.getProperty(KEEP_EMPTY_REGIONS_KEY) != null) {
			setKeepEmptyRegions(trpProps.getBoolProperty(KEEP_EMPTY_REGIONS_KEY));
		}
		if (props.getProperty(SPLIT_LINES_KEY) != null) {
			setSplitLines(trpProps.getBoolProperty(SPLIT_LINES_KEY));
		}
		if (props.getProperty(LINE_OVERLAP_FRACTION_KEY) != null) {
			setLineOverlapFraction(trpProps.getDoubleProperty(LINE_OVERLAP_FRACTION_KEY));
		}
		if (props.getProperty(CLUSTER_LINES_WITHOUT_REGIONS_KEY) != null) {
			setClusterLinesWithoutRegions(trpProps.getBoolProperty(CLUSTER_LINES_WITHOUT_REGIONS_KEY));
		}
	}    

    public void setAddToPageXML(boolean addToPageXML) {
        this.addToPageXML = addToPageXML;
    }

    public boolean getAddToPageXML() {
        return addToPageXML;
    }
    
    public void setApproxPolyFrac(Double approxPolyFrac) {
        this.approxPolyFrac = approxPolyFrac;
    }

    public Double getApproxPolyFrac() {
        return approxPolyFrac;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public Double getThreshold() {
        return threshold;
    }

	public void setKeepEmptyRegions(boolean keepEmptyRegions) {
		this.keepEmptyRegions = keepEmptyRegions;
	}

	public boolean getKeepEmptyRegions() {
		return keepEmptyRegions;
	}

	public void setSplitLines(boolean splitLines) {
		this.splitLines = splitLines;
	}

	public boolean getSplitLines() {
		return splitLines;
	}

	public void setLineOverlapFraction(Double lineOverlapFraction) {
		this.lineOverlapFraction = lineOverlapFraction;
	}

	public Double getLineOverlapFraction() {
		return lineOverlapFraction;
	}

	public void setClusterLinesWithoutRegions(boolean clusterLinesWithoutRegions) {
		this.clusterLinesWithoutRegions = clusterLinesWithoutRegions;
	}

	public boolean getClusterLinesWithoutRegions() {
		return clusterLinesWithoutRegions;
	}

	public void setEnrichExistingTranscriptions(boolean enrichExistingTranscriptions) {
		props.setProperty(ENRICH_EXISTING_TRANSCRIPTIONS_KEY, "" + enrichExistingTranscriptions);
	}

	public boolean isEnrichExistingTranscriptions() {
		return props.getProperty(ENRICH_EXISTING_TRANSCRIPTIONS_KEY) != null ? Boolean.parseBoolean(props.getProperty(ENRICH_EXISTING_TRANSCRIPTIONS_KEY)) : false;
	}

	public Properties getProps() {
		return props;
	}

	/**
	 * Returns a TrpProperties object with human readable property names for each config parameter
	 */
	public TrpProperties toParamProps() {
		TrpProperties p = new TrpProperties();
		p.setProperty(LA2InferenceConfig.APPROX_POLY_FRAC_KEY, "" + approxPolyFrac);
        p.setProperty(LA2InferenceConfig.ADD_TO_PAGE_XML_KEY, addToPageXML);
		p.setProperty(LA2InferenceConfig.THRESHOLD_KEY, "" + threshold);
		// if (advancedParams!=null) {
		// 	for (String key : advancedParams.getParamMap().keySet()) {
		// 		p.setProperty(key, advancedParams.getParameterValue(key));		
		// 	}
		// }	
		return p;
	}

	@Override
	public String toString() {
		return "LA2InferenceConfig [approxPolyFrac=" + approxPolyFrac + ", addToPageXML=" + addToPageXML + ", threshold=" + threshold + "]";			
	}

	// @Override
	// public String getType() {
	// 	return TrpHtr.TYPE_LAYOUT;
	// }


}

