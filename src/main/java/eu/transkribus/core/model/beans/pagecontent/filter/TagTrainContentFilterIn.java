package eu.transkribus.core.model.beans.pagecontent.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.customtags.CustomTag;
import eu.transkribus.core.model.beans.customtags.CustomTagList;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent.TextRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextLineType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextRegionType;
import eu.transkribus.core.util.PageXmlUtils;
import eu.transkribus.core.util.UnicodeList;

/**
 * Filter that filters custom tags, creates special strings around them for training
 * <br>
 * Use case: Users want to train tags
 */
public class TagTrainContentFilterIn implements IPageContentFilter {
	Logger logger = LoggerFactory.getLogger(TagTrainContentFilterIn.class);
	
	/**
	 * tagsToTrain: list that contains the tags which should be filtered
	 * tagMap: mapping of tag name and used symbol for this tag
	 * propMap: contains the stored properties (as cssString) and the replacement symbols (Pair of two symbols, contained in 'propertyLabels')
	 * propertyLabels: list of special chars combinations of size two to have enough for each tag property
	 */
	public boolean trainAbbrevs = false;
	public boolean replaceProps = false;
	public boolean useShortForm = false;
	public List<String> tagsToTrain = new ArrayList<>();
	private Map<String, Object> tagMap = new HashMap<String, Object>();	
	private static Map<String, String> propMap = new HashMap<String, String>();
	private static Map<String, Object> propMapToStore = new HashMap<String, Object>();
	private List<String> propertyLabels = new ArrayList<String>();
	
	private static int index = 0;
	private static int countTags = 0;
	
	/*
	 * 
	 * 
	 * LocalRecognitionJobManager: line 82: store props into DB and HTR model
	 * ACITlabHtrTrainingJob: trpTrainer.storeTrainAndValInput(); da werden die Filter gesetzt
	 * htr = loadDataFromFileSystem(htr); und hier werden die params zum Modell gespeichert für die Erkennung
	 */
	
	
	
	/*
	 * für properties 2 unicode strings kombinieren -> ergibt genügend verschiedene mappings
	 * 640 * 640 = 409.000 possibilities
	 */
	UnicodeList unicodes4Properties = new UnicodeList("aboriginalSyllabics", "U+1400-U+167F");
	
	public TagTrainContentFilterIn(boolean useProps, boolean shortForm, boolean trainAbbrevs, List<String> tagsToTrain, Map<String, Object> tagMap2) {
		this(useProps, shortForm, trainAbbrevs, tagMap2, tagsToTrain.toArray(new String[0]));
	}
	
	public TagTrainContentFilterIn(boolean useProps, boolean shortForm, boolean trainAbbrevs, Map<String, Object> tagMap2, String...tags) {
		super();
		this.replaceProps = useProps;
		this.useShortForm = shortForm;
		this.trainAbbrevs = trainAbbrevs;
		for (String s : tags) {
			tagsToTrain.add(s);
		}
		this.tagMap = tagMap2;
		if(useShortForm)
			initPropertyLabels();

	}

	/*
	 * list of special chars combinations of size two to have enough for each tag property
	 */
	private void initPropertyLabels() {
		for (int i = 0; i<unicodes4Properties.getUnicodes().size(); i++) {
			for (int j = 0; j<unicodes4Properties.getUnicodes().size(); j++) {
				propertyLabels.add(new String(unicodes4Properties.getUnicodes().get(i).getRight() + unicodes4Properties.getUnicodes().get(j).getRight()));
			}
		}
		logger.debug("number of possible property values: " + propertyLabels.size());
	}

	@Override
	public void doFilter(PcGtsType pc) {

		List<TextRegionType> regions = PageXmlUtils.getTextRegions(pc);
		for(TextRegionType r : regions) {
			Iterator<TrpTextLineType> lineIt = ((TrpTextRegionType) r).getTrpTextLine().iterator();
			while(lineIt.hasNext()) {
				TrpTextLineType l = lineIt.next();
				//What's the difference between getTagList() and getCustomTagList()?
				CustomTagList tagList = l.getCustomTagList();
				List<CustomTag> ct = tagList.getIndexedTags();
 
				Collections.sort(ct, new SortByOffset());
				
//		        logger.debug("\nSorted by offset");
//		        for (int i = 0; i < ct.size(); i++) {
//		        	 //logger.debug(ct.get(i).getTagName());
//		        	logger.debug(ct.get(i).getOffset());
//		        }
		            			
				//use strings to discriminate tags. No easy way for doing this by CustomTag type equivalence!?
				for(CustomTag t : ct) {
					countTags++;
					logger.debug("Checking tag: {}", t.getTagName());
					if(tagMap.containsKey(t.getTagName())) {
						//String delimiter = mappingMap.get(t.getTagName()) != null ? mappingMap.get(t.getTagName()) : "$";
						logger.debug("Found tag {}: {}", l.getId(), t.getTagName());
						//tagMap.put(t.getTagName(), t.getContainedText());

						String propLabel = null;
						
						if(replaceProps && useShortForm && t.getAttributeCssStr()!="") {
							String cssString = "{"+t.getAttributeCssStr()+"}";
							if (propMap.containsKey(cssString)) {
								propLabel = propMap.get(cssString);
							}
							else {
								propLabel = propertyLabels.get(index++);
								//ToDo: test the getAttributeCssStr
								propMap.put(cssString, propLabel);
							}
						}
						logger.debug("label for props of this tag: " + propLabel);

						PageXmlUtils.convertCustomTagIntoSpecialString(l, t, (String) tagMap.get(t.getTagName()), propLabel, replaceProps, useShortForm, trainAbbrevs);
					}
				}
			}
		}
		
		for (String i : propMap.keySet()) {
			String j = propMap.get(i);
			logger.debug("propMap size is: " + propMap.size());
			logger.debug("number of tags is: " + countTags);
			logger.debug("key is: " + i + " and its value is: " + j);
			
		}
 
	}
	

	
//	public static String getCharacter(String tagName) {
//		// TODO Auto-generated method stub
//		return tagMap.get(tagName);
//	}

//	public List<String> getUnicodes() {
//		return unicodes4CustomTags.getUnicodesAsStrings();
//	}
	
	
	public Pair<String, String> getUnicode4Prop(int i, int j) {
		String left = unicodes4Properties.getUnicodesAsStrings().get(i);
		String right = unicodes4Properties.getUnicodesAsStrings().get(j);
		return new ImmutablePair<>(left, right);
	}
	
	class SortByOffset implements Comparator<CustomTag> {
	    // Used for sorting in desscending order of
	    // offset number
	    public int compare(CustomTag a, CustomTag b)
	    {
	        return b.getOffset() - a.getOffset();
	    }
	}
	
	/*
	 * during filtering property map is as following:
	 * key: cssString of all the properties: t.getAttributeCssStr()
	 * value: combination of two special symbols
	 * aim: have one entry for each expression of properties
	 * 
	 * for the recognition job we need it the other way round: HTR recognices the value: hence we sort it the other way round
	 */
	public Map<String, Object> getPropMap(){

		for (String key : propMap.keySet()) {
			String v = propMap.get(key);
			propMapToStore.put(v, key);
		}
		
		return propMapToStore;
	}
	
}
