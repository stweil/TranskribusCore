package eu.transkribus.core.model.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CREDIT_HISTORY")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpCreditHistoryEntry {
	
	public TrpCreditHistoryEntry() {}
	
	@Transient
	@Id
	@Column(name = "ID")
	private Integer historyId;

	@Column(name = "TIME")
	private Date time;
	
	//the user responsible might be different from source AND target
	@Column(name = "USER_ID")
	private Integer userId;
	
	@Column(name = "USERNAME")
	private String userName;
	
	@Column(name = "SOURCE_COLLECTION_ID")
	private Integer sourceCollectionId;
	
	@Column(name = "SOURCE_USER_ID")
	private Integer sourceUserId;
	
	@Column(name = "SOURCE_NAME")
	private String sourceName;
	
	@Column(name = "TARGET_COLLECTION_ID")
	private Integer targetCollectionId;
	
	@Column(name = "TARGET_USER_ID")
	private Integer targetUserId;
	
	@Column(name = "TARGET_NAME")
	private String targetName;
	
	@Column(name = "JOBID")
	private Integer jobId;
	
	@Column(name = "CREDIT_VALUE")
	private double creditValue;
	
	/**
	 * Optional link to CREDIT_COSTS entry applied regarding costFactor
	 */
	@Column(name = "COSTS_ID")
	private Integer costsId;
	
	@Column(name = "COST_FACTOR")
	private Double costFactor;
	
	@Column(name = "NR_OF_PAGES")
	private Double nrOfPages;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Transient
	@Column(name = "SOURCE")
	private TrpCreditTransactionSubject source;
	
	@Transient
	@Column(name = "TARGET")
	private TrpCreditTransactionSubject target;

	@Column(name = "PACKAGE_EXPIRATION_DATE")
	private Date packageExpirationDate;

	public Integer getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Integer historyId) {
		this.historyId = historyId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getSourceCollectionId() {
		return sourceCollectionId;
	}

	public void setSourceCollectionId(Integer sourceCollectionId) {
		this.sourceCollectionId = sourceCollectionId;
	}

	public Integer getSourceUserId() {
		return sourceUserId;
	}

	public void setSourceUserId(Integer sourceUserId) {
		this.sourceUserId = sourceUserId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Integer getTargetCollectionId() {
		return targetCollectionId;
	}

	public void setTargetCollectionId(Integer targetCollectionId) {
		this.targetCollectionId = targetCollectionId;
	}

	public Integer getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(Integer targetUserId) {
		this.targetUserId = targetUserId;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public double getCreditValue() {
		return creditValue;
	}

	public void setCreditValue(double creditValue) {
		this.creditValue = creditValue;
	}

	public Integer getCostsId() {
		return costsId;
	}

	public void setCostsId(Integer costsId) {
		this.costsId = costsId;
	}

	public Double getCostFactor() {
		return costFactor;
	}

	public void setCostFactor(Double costFactor) {
		this.costFactor = costFactor;
	}

	public Double getNrOfPages() {
		return nrOfPages;
	}

	public void setNrOfPages(Double nrOfPages) {
		this.nrOfPages = nrOfPages;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TrpCreditTransactionSubject getSource() {
		return source;
	}

	public void setSource(TrpCreditTransactionSubject source) {
		this.source = source;
	}

	public TrpCreditTransactionSubject getTarget() {
		return target;
	}

	public void setTarget(TrpCreditTransactionSubject target) {
		this.target = target;
	}

	public Date getPackageExpirationDate() {
		return packageExpirationDate;
	}

	public void setPackageExpirationDate(Date packageExpirationDate) {
		this.packageExpirationDate = packageExpirationDate;
	}
	
	@Override
	public String toString() {
		return "TrpCreditHistoryEntry [historyId=" + historyId + ", time=" + time + ", sourceCollectionId="
				+ sourceCollectionId + ", sourceUserId=" + sourceUserId + ", sourceName=" + sourceName
				+ ", targetCollectionId=" + targetCollectionId + ", targetUserId=" + targetUserId + ", targetName="
				+ targetName + ", jobId=" + jobId + ", creditValue=" + creditValue + ", costsId=" + costsId
				+ ", costFactor=" + costFactor + ", nrOfPages=" + nrOfPages + ", description=" + description
				+ ", source=" + source + ", target=" + target + ", packageExpirationDate=" + packageExpirationDate
				+ "]";
	}
	
	/**
	 * @deprecated
	 * Most probably not needed
	 */
	public static class TrpCreditTransactionSubject {
		String type;
		Integer id;
		String name;
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TrpCreditTransactionSubject other = (TrpCreditTransactionSubject) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return "TrpCreditTransactionSubject [type=" + type + ", id=" + id + ", name=" + name + "]";
		}
	}
}
