package eu.transkribus.core.model.beans.searchresult;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TagHit {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TagHit.class);

    String id;
    long dId;
    String title;
    List<Integer> colIds;
    long pageId;
    long pageNr;
    String lId;
    String value;
    String type;
    String contextBefore;
    String contextAfter;
    Map<String,String> customAttributes; 

    public TagHit(){}

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDId() {
        return this.dId;
    }

    public void setDId(long dId) {
        this.dId = dId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getColIds() {
        return this.colIds;
    }

    public void setColIds(List<Integer> colId) {
        this.colIds = colId;
    }

    public long getPageId() {
        return this.pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public long getPageNr() {
        return this.pageNr;
    }

    public void setPageNr(long pageNr) {
        this.pageNr = pageNr;
    }

    public String getLId() {
        return this.lId;
    }

    public void setLId(String lId) {
        this.lId = lId;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContextBefore() {
        return this.contextBefore;
    }

    public void setContextBefore(String contextBefore) {
        this.contextBefore = contextBefore;
    }

    public String getContextAfter() {
        return this.contextAfter;
    }

    public void setContextAfter(String contextAfter) {
        this.contextAfter = contextAfter;
    }

    public Map<String,String> getCustomAttributes() {
        return this.customAttributes;
    }

    public void setCustomAttributes(Map<String,String> customAttributes) {
        this.customAttributes = customAttributes;
    }

	@Override
	public String toString() {
		return "TagHit [id=" + id + ", dId=" + dId + ", title=" + title + ", colIds=" + colIds + ", pageId=" + pageId
				+ ", pageNr=" + pageNr + ", lId=" + lId + ", value=" + value + ", type=" + type + ", context_before="
				+ contextBefore + ", context_after=" + contextAfter + ", customAttributes=" + customAttributes + "]";
	}
}


