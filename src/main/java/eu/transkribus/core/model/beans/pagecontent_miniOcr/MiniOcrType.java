package eu.transkribus.core.model.beans.pagecontent_miniOcr;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ocr", namespace="")
public class MiniOcrType {


    MiniOcrPageType page;

    @XmlElement(name = "p")
    public void setPage(MiniOcrPageType page) {
        this.page = page;
    }

    public MiniOcrPageType getPage(){
        return this.page;
    }
    
}


