package eu.transkribus.core.model.beans.pagecontent_miniOcr;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "l")
public class MiniOcrLineType {

    @XmlAttribute(name= "id")
    String lineId;

    @XmlElement(name = "w")
    List<MiniOcrWordType> words;

    public void setWords(List<MiniOcrWordType> words){
        this.words = words;
    }

    public void setLineId(String lineId){
        this.lineId = lineId;
    }
    
}
