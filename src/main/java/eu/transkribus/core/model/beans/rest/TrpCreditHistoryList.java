package eu.transkribus.core.model.beans.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import eu.transkribus.core.model.beans.TrpCreditHistoryEntry;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpCreditHistoryEntry.class})
public class TrpCreditHistoryList extends JaxbPaginatedList<TrpCreditHistoryEntry> {
	@XmlAttribute
	private Double overallBalance;
	@XmlAttribute
	private Double personalBalance;
	@XmlAttribute
	private Double shareableBalance;

	public TrpCreditHistoryList() {
		super();
	}

	public TrpCreditHistoryList(List<TrpCreditHistoryEntry> list, int total, int index, int nValues, String sortColumnField,
			String sortDirection) {
		super(list, total, index, nValues, sortColumnField, sortDirection);
	}
	
	public TrpCreditHistoryList(List<TrpCreditHistoryEntry> list, int total, Double overallBalance, int index, int nValues, String sortColumnField,
			String sortDirection) {
		this(list, total, index, nValues, sortColumnField, sortDirection);
		this.overallBalance = overallBalance;
	}
	
	public Double getOverallBalance() {
		return overallBalance;
	}

	public void setOverallBalance(Double overallBalance) {
		this.overallBalance = overallBalance;
	}

	public Double getPersonalBalance() {
		return personalBalance;
	}

	public void setPersonalBalance(Double personalBalance) {
		this.personalBalance = personalBalance;
	}

	public Double getShareableBalance() {
		return shareableBalance;
	}

	public void setShareableBalance(Double shareableBalance) {
		this.shareableBalance = shareableBalance;
	}
}
