package eu.transkribus.core.model.beans;

import eu.transkribus.core.model.beans.auth.TrpRole;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name="COLLECTION_INVITES")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpCollectionInvite {

    @Id
    @Column(name = "invite_id")
    private Integer inviteId;
    @Column(name = "inviting_user_id")
    private int invitingUserId;
    @Column
    @Transient
    private String invitingUserName;
    @Column
    @Transient
    private String invitingUserFirstname;
    @Column
    @Transient
    private String invitingUserLastname;
    @Column(name = "invitee_email")
    private String inviteeEmail;
    @Column(name = "collection_id")
    private Integer colId;
    @Column
    @Transient
    private String colName;
    @Column(name = "role")
    private TrpRole role;
    @Column(name = "created")
    private Date created;
    @Column(name = "state")
    private State state;
    
    private String message;

    public TrpCollectionInvite() {}

    public Integer getInviteId() {
        return inviteId;
    }

    public void setInviteId(Integer inviteId) {
        this.inviteId = inviteId;
    }

    public int getInvitingUserId() {
        return invitingUserId;
    }

    public void setInvitingUserId(int invitingUserId) {
        this.invitingUserId = invitingUserId;
    }

    public String getInvitingUserName() {
        return invitingUserName;
    }

    public void setInvitingUserName(String invitingUserName) {
        this.invitingUserName = invitingUserName;
    }

    public String getInvitingUserFirstname() {
        return invitingUserFirstname;
    }

    public void setInvitingUserFirstname(String invitingUserFirstname) {
        this.invitingUserFirstname = invitingUserFirstname;
    }

    public String getInvitingUserLastname() {
        return invitingUserLastname;
    }

    public void setInvitingUserLastname(String invitingUserLastname) {
        this.invitingUserLastname = invitingUserLastname;
    }

    public String getInviteeEmail() {
        return inviteeEmail;
    }

    public void setInviteeEmail(String inviteeEmail) {
        this.inviteeEmail = inviteeEmail;
    }

    public Integer getColId() {
        return colId;
    }

    public void setColId(Integer colId) {
        this.colId = colId;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public TrpRole getRole() {
        return role;
    }

    public void setRole(TrpRole role) {
        this.role = role;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TrpCollectionInvite{" +
                "inviteId=" + inviteId +
                ", invitingUserId=" + invitingUserId +
                ", invitingUserName='" + invitingUserName + '\'' +
                ", invitingUserFirstname='" + invitingUserFirstname + '\'' +
                ", invitingUserLastname='" + invitingUserLastname + '\'' +
                ", inviteeEmail='" + inviteeEmail + '\'' +
                ", colId=" + colId +
                ", colName='" + colName + '\'' +
                ", role=" + role +
                ", created=" + created +
                ", state = '" + state + "'" +
                ", message = '" + message + "'" +
                '}';
    }

    public enum State {
        PENDING,
        ACCEPTED,
        REJECTED,
    }
}
