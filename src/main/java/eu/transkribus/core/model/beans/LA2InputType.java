package eu.transkribus.core.model.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonPropertyOrder({"input_type", "tag_value", "type"})
public class LA2InputType implements Serializable {
	@Schema(description = "input_type", required=true)
	private InputType input_type;

	@Schema(description = "tag_value", required=true)
	private String tag_value;

	@Schema(description = "type", required=true)
	private Type type;

    public void setInputType(InputType input_type) {
        this.input_type = input_type;
    }

    public InputType getInputType() {
        return input_type;
    }

    public void setTagValue(String tag_value) {
        this.tag_value = tag_value;
    }

    public String getTagValue() {
        return tag_value;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String toString() {
        return "input_type: " + input_type + " tag_value " + tag_value + " type: " + type;
    }

    public enum InputType {
        TEXT_REGION,
        TEXT_LINE,
        SEPARATOR,
        GRAPHIC_REGION,
        IMAGE_REGION,
        BASE_LINE,
        WORD;
    }

    public enum Type {
        tag_obj, page_obj;
    }
}