package eu.transkribus.core.model.beans;

import eu.transkribus.core.util.ModelUtil;

public class HtrUsageStats {
	Integer userId;
	String username;
	String modelProvider;
	Integer modelDocType;
	Integer day;
	Integer week;
	Integer month;
	Integer year;
	Integer nrOfJobs;
	Integer nrOfPages;
	Integer nrOfCharacters;
	public HtrUsageStats() {}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getModelProvider() {
		return modelProvider;
	}
	public void setModelProvider(String modelProvider) {
		this.modelProvider = modelProvider;
	}
	public String getModelProviderLabel() {
		if(ModelUtil.PROVIDER_CITLAB_PLUS.equals(getModelProvider())) {
			return "HTR+";
		}
		return getModelProvider();
	}
	public Integer getModelDocType() {
		return modelDocType;
	}
	public void setModelDocType(Integer modelDocType) {
		this.modelDocType = modelDocType;
	}
	public String getModelDocTypeLabel() {
		return modelDocType < 1 ? "Handwritten" : "Print";
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getWeek() {
		return week;
	}
	public void setWeek(Integer week) {
		this.week = week;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNrOfJobs() {
		return nrOfJobs;
	}
	public void setNrOfJobs(Integer nrOfJobs) {
		this.nrOfJobs = nrOfJobs;
	}
	public Integer getNrOfPages() {
		return nrOfPages;
	}
	public void setNrOfPages(Integer nrOfPages) {
		this.nrOfPages = nrOfPages;
	}
	public Integer getNrOfCharacters() {
		return nrOfCharacters;
	}
	public void setNrOfCharacters(Integer nrOfCharacters) {
		this.nrOfCharacters = nrOfCharacters;
	}
	@Override
	public String toString() {
		return "HtrUsageStats [userId=" + userId + ", username=" + username + ", modelProvider=" + modelProvider
				+ ", modelDocType=" + modelDocType + ", day=" + day + ", week=" + week + ", month=" + month + ", year="
				+ year + ", nrOfJobs=" + nrOfJobs + ", nrOfPages=" + nrOfPages + ", nrOfCharacters=" + nrOfCharacters + "]";
	}
}
