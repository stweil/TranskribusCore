package eu.transkribus.core.model.beans.enums;

import org.apache.commons.lang.StringUtils;

public enum DocType {
	UNDEFINED(null),
	HANDWRITTEN(0),
	PRINT(1);
	
	private Integer value;
	
	private DocType(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}
	
	/**
	 * Resolve the enum type for an int value. If int value is not mapped, null is returned.
	 * 
	 * @param value
	 * @return
	 */
	public static DocType fromValue(final Integer value) {
		DocType result = UNDEFINED;
		if(value == null) {
			return result;
		}
		for(DocType t : DocType.values()) {
			if(value.equals(t.getValue())) {
				result = t;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Resolve the enum type for an int value. If int value is not mapped, null is returned.
	 * 
	 * @param value
	 * @return
	 */
	public static DocType fromString(final String string) {
		DocType result = UNDEFINED;
		if(StringUtils.isEmpty(string)) {
			return result;
		}
		for(DocType t : DocType.values()) {
			if(StringUtils.equalsIgnoreCase(string, t.toString())) {
				result = t;
				break;
			}
		}
		return result;
	}
}
