package eu.transkribus.core.model.beans.pagecontent.filter;

import eu.transkribus.core.model.beans.TrpStructureStats;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.PageXmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

/**
 * {@link IPageContentFilter} implementation for aggregating structure stats from {@link PcGtsType} objects.
 */
public class StructureStatisticsFilter implements IPageContentFilter {
    private static final Logger logger = LoggerFactory.getLogger(StructureStatisticsFilter.class);

    private final TrpStructureStats stats;

    public StructureStatisticsFilter() {
        this(new TrpStructureStats());
    }

    public StructureStatisticsFilter(TrpStructureStats initStats) {
        if(initStats == null) {
            stats = new TrpStructureStats();
        } else {
            stats = initStats;
        }
    }

    @Override
    public void doFilter(PcGtsType pc) {
        TrpStructureStats stats = PageXmlUtils.extractRegionStructStats(pc, true);
        logger.debug("Extracted structure stats from PcGtsType for image '{}': {}", pc.getPage().getImageFilename(), stats);
        add(this.stats, stats);
        logger.debug("New overall stats: {}", this.stats);
    }

    private void add(TrpStructureStats overallStats, TrpStructureStats statsToAdd) {
        for(TrpStructureStats.RegionTypeStats e : statsToAdd.getRegionTypes()) {
            Optional<TrpStructureStats.RegionTypeStats> typedStats = overallStats.getRegionTypes().stream()
                    .filter(t -> Objects.equals(e.getRegionType(), t.getRegionType()))
                    .findFirst();

            if(typedStats.isPresent()) {
                add(typedStats.get(), e);
            } else {
                overallStats.getRegionTypes().add(new TrpStructureStats.RegionTypeStats(e));
            }
        }
    }

    private void add(TrpStructureStats.RegionTypeStats overallStats, TrpStructureStats.RegionTypeStats regionTypeStatsToAdd) {
        overallStats.setOverallCount(overallStats.getOverallCount() + regionTypeStatsToAdd.getOverallCount());
        for(TrpStructureStats.StructureType t : regionTypeStatsToAdd.getStructureTypes()) {
            Optional<TrpStructureStats.StructureType> type = overallStats.getStructureTypes().stream()
                    .filter(s -> Objects.equals(s.getName(), t.getName()))
                    .findFirst();

            if(type.isPresent()) {
                type.get().setCount(type.get().getCount() + t.getCount());
            } else {
                overallStats.getStructureTypes().add(new TrpStructureStats.StructureType(t));
            }
        }
    }

    public TrpStructureStats getStats() {
        return stats;
    }
}
