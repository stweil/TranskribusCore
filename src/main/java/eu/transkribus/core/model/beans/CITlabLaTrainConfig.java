package eu.transkribus.core.model.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CITlabLaTrainConfig extends HtrTrainConfig implements Serializable {
	private static final long serialVersionUID = 8863676001947093658L;
	
	public static final int DEFAULT_NUM_EPOCHS = 100;
	public static final double DEFAULT_LEARNING_RATE = 0.001d;
	public static final String DEFAULT_MODEL_TYPE = "ru";
	
	@Hidden
	public final static String MODEL_TYPE_KEY = "Model Type";	
	
	@Schema(description = "The number of epochs to be trained. A positive natural number.", required=true, defaultValue = ""+DEFAULT_NUM_EPOCHS)
	protected Integer numEpochs = DEFAULT_NUM_EPOCHS;
	@Schema(description = "The learning rate for traininig. Floating point in scientific notation, e.g. 2E-3", required=false, defaultValue = ""+DEFAULT_LEARNING_RATE)
	protected Double learningRate=DEFAULT_LEARNING_RATE;
	@Schema(description = "The model type - one of: 'u', 'ru', 'aru', 'laru' - cf. https://arxiv.org/pdf/1802.03345v2.pdf ", required=false, defaultValue = DEFAULT_MODEL_TYPE)
	protected String modelType = DEFAULT_MODEL_TYPE;
	@Schema(description = "A map with advanced parameter", required=false)
	protected ParameterMap advancedParams;
	
	public CITlabLaTrainConfig() {
		super();
	}

	public Integer getNumEpochs() {
		return numEpochs==null ? DEFAULT_NUM_EPOCHS : numEpochs;
	}

	public void setNumEpochs(Integer numEpochs) {
		this.numEpochs = numEpochs;
	}

	public Double getLearningRate() {
		return learningRate==null ? DEFAULT_LEARNING_RATE : learningRate;
	}

	public void setLearningRate(Double learningRate) {
		this.learningRate = learningRate;
	}

	public String getModelType() {
		return modelType==null ? DEFAULT_MODEL_TYPE : modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	
	public ParameterMap getAdvancedParams() {
		return advancedParams;
	}

	public void setAdvancedParams(ParameterMap advancedParams) {
		this.advancedParams = advancedParams;
	}

	/**
	 * Returns a TrpProperties object with human readable property names for each config parameter
	 */
	public TrpProperties toParamProps() {
		TrpProperties p = new TrpProperties();
		p.setProperty(HtrTrainConfig.NUM_EPOCHS_KEY, numEpochs);
		p.setProperty(HtrTrainConfig.LEARNING_RATE_KEY, ""+learningRate);
		p.setProperty(CITlabLaTrainConfig.MODEL_TYPE_KEY, modelType);
		if (advancedParams!=null) {
			for (String key : advancedParams.getParamMap().keySet()) {
				p.setProperty(key, advancedParams.getParameterValue(key));		
			}
		}	
		return p;
	}

	@Override
	public String toString() {
		return "LaTrainConfig [numEpochs=" + numEpochs + ", learningRate=" + learningRate + ", modelType=" + modelType
				+ (advancedParams==null ? "" : ", advancedParams=("+advancedParams.toSingleLineString()+")") + "]";			
	}

	@Override
	public String getType() {
		return TrpHtr.TYPE_LAYOUT;
	}
	
}
