package eu.transkribus.core.model.beans.auth;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import eu.transkribus.core.model.beans.TrpUserCollection;
import eu.transkribus.core.model.beans.adapters.SqlTimestampAdapter;

@Entity
@Table(name = "USERS")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpUser implements Serializable {
	private static final long serialVersionUID = 2370532427197634733L;

	@Id
	@Column(name = "USER_ID")
	protected int userId;
	@Column(name = "KC_USER_ID")
	private String kcUserId;
	@Column(name = "USERNAME")
	protected String userName;
	@Column(name = "EMAIL")
	protected String email;
	@Column(name = "LOCALE")
	private String locale;
	protected Integer affiliationId;
	protected String affiliation;
	@Column(name = "FIRSTNAME")
	protected String firstname;
	@Column(name = "LASTNAME")
	protected String lastname;
	protected String gender;
	protected String orcid;
	protected String profilePicUrl;
	
	protected List<String> userRoleList;
	
	protected int isActive=1;
	
	/**
	 * userRoleList is now used instead of this flag. Still here for backwards compatibility during transition
	 */
	protected boolean isAdmin = false;

	@Column(name = "CREATED")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp created = null;

	@Column(name = "DEL_TIME")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp delTime = null;

	@Column(name = "LAST_UPDATE")
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	private Timestamp lastUpdate;
		
	@XmlTransient // shall not be transferred!
	protected String password;
	@XmlTransient
	protected String hash;
	
	// gets set when retrieving user for collection
	protected TrpUserCollection userCollection = null; 
	
	public TrpUser(){}
	
	/**
	 * Copy Constructor
	 * @param trpUser a <code>TrpUser</code> object
	 */
	public TrpUser(TrpUser trpUser) {
	    this.userId = trpUser.userId;
	    this.userName = trpUser.userName;
	    this.email = trpUser.email;
	    this.affiliation = trpUser.affiliation;
	    this.firstname = trpUser.firstname;
	    this.lastname = trpUser.lastname;
	    this.gender = trpUser.gender;
	    this.orcid = trpUser.orcid;
	    this.isAdmin = trpUser.isAdmin;
	    this.password = trpUser.password;
	    this.isActive = trpUser.isActive;
	    this.created = trpUser.created;
	    this.delTime = trpUser.delTime;
	    this.profilePicUrl = trpUser.profilePicUrl;
	    this.userRoleList = trpUser.userRoleList;
		this.kcUserId = trpUser.getKcUserId();
		this.locale = trpUser.getLocale();
		this.lastUpdate = trpUser.getLastUpdate();
	}

	public TrpUser(final int userId, final String userName){
		this.userId = userId;
		this.userName = userName;
	}
	
	public TrpUserCollection getUserCollection() {
		return userCollection;
	}

	public void setUserCollection(TrpUserCollection userCollection) {
		this.userCollection = userCollection;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;			
	}	
	
	public List<String> getUserRoleList() {
		return userRoleList;
	}
	
	public void setUserRoleList(List<String> userRoleList) {
		this.userRoleList = userRoleList;
		
		if(userRoleList != null && userRoleList.contains("Admin")) {
			setAdmin(true);
		}
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	
	public Integer getAffiliationId() {
		return affiliationId;
	}

	public void setAffiliationId(Integer affiliationId) {
		this.affiliationId = affiliationId;
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getOrcid() {
		return orcid;
	}
	
	public void setOrcid(String orcid){
		this.orcid = orcid;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	public Timestamp getDelTime()  {
		return delTime;
	}
	
	public void setDelTime(Timestamp delTime) {
		this.delTime = delTime;
	}

	public String getKcUserId() {
		return kcUserId;
	}

	public void setKcUserId(String kcUserId) {
		this.kcUserId = kcUserId;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getInfo(boolean withEmail) {
		String str = firstname+" "+lastname;
		if (withEmail)
			str += " "+email;
		
		return str;
	}
	
	public TrpRole getRoleInCollection() {
		TrpUserCollection uc = getUserCollection();
		return (uc == null ? TrpRole.None : (uc.getRole() == null ? TrpRole.None : uc.getRole()));
	}	
	
	public String getFullname() {
		return firstname+" "+lastname;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TrpUser)) return false;
		TrpUser trpUser = (TrpUser) o;
		return userId == trpUser.userId && isActive == trpUser.isActive && isAdmin == trpUser.isAdmin && Objects.equals(kcUserId, trpUser.kcUserId) && Objects.equals(userName, trpUser.userName) && Objects.equals(email, trpUser.email) && Objects.equals(locale, trpUser.locale) && Objects.equals(affiliationId, trpUser.affiliationId) && Objects.equals(affiliation, trpUser.affiliation) && Objects.equals(firstname, trpUser.firstname) && Objects.equals(lastname, trpUser.lastname) && Objects.equals(gender, trpUser.gender) && Objects.equals(orcid, trpUser.orcid) && Objects.equals(profilePicUrl, trpUser.profilePicUrl) && Objects.equals(userRoleList, trpUser.userRoleList) && Objects.equals(created, trpUser.created) && Objects.equals(delTime, trpUser.delTime) && Objects.equals(lastUpdate, trpUser.lastUpdate) && Objects.equals(password, trpUser.password) && Objects.equals(hash, trpUser.hash) && Objects.equals(userCollection, trpUser.userCollection);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId, kcUserId, userName, email, locale, affiliationId, affiliation, firstname, lastname, gender, orcid, profilePicUrl, userRoleList, isActive, isAdmin, created, delTime, lastUpdate, password, hash, userCollection);
	}

	@Override
	public String toString() {
		return "TrpUser{" +
				"userId=" + userId +
				", kcUserId='" + kcUserId + '\'' +
				", userName='" + userName + '\'' +
				", email='" + email + '\'' +
				", locale='" + locale + '\'' +
				", affiliationId=" + affiliationId +
				", affiliation='" + affiliation + '\'' +
				", firstname='" + firstname + '\'' +
				", lastname='" + lastname + '\'' +
				", gender='" + gender + '\'' +
				", orcid='" + orcid + '\'' +
				", profilePicUrl='" + profilePicUrl + '\'' +
				", userRoleList=" + userRoleList +
				", isActive=" + isActive +
				", isAdmin=" + isAdmin +
				", created=" + created +
				", delTime=" + delTime +
				", lastUpdate=" + lastUpdate +
				", password='" + password + '\'' +
				", hash='" + hash + '\'' +
				", userCollection=" + userCollection +
				'}';
	}
}
