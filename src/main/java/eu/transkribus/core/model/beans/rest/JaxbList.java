package eu.transkribus.core.model.beans.rest;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.transkribus.core.model.beans.searchresult.PageHit;
import eu.transkribus.core.util.GsonUtil;
import eu.transkribus.core.util.JaxbUtils;

@XmlRootElement(name="List")
@JsonRootName("List")
public class JaxbList<T>{
	
    protected List<T> list;

    public JaxbList(){
    	this.list = new ArrayList<T>();
    }

    public JaxbList(List<T> list){
    	this.list=list;
    }

    @XmlElement(name="Item")
    @JsonProperty("Item")
    public List<T> getList(){
    	return list;
    }
    
    public void setList(List<T> list){
    	this.list=list;
    }
    
    public void add(T e) {
    	this.list.add(e);
    }
    
    public void remove(T o) {
    	this.list.remove(o);
    }
    
    public static void main(String[] args) throws Exception {
    	try {
			List<Integer> l = new ArrayList<>();
			l.add(1);
			l.add(3);
			l.add(6);
			
			JaxbList<PageHit> phList = new JaxbList<PageHit>();
			
			String jsonParsStr = "{\r\n" + 
					"   \"Item\" : [ \"PageHit [docId=39931, pageNr=1, pageUrl=null, highlights=[<em>schorsch</em> Auentinus ſtudioſis. Grammatice. Salutem. et ingenuos laboꝛes. ¶ Quãuis multa variaqꝫ a], wordCoords=[schorsch:r_1_1/tl_1/tl_1_1:457,290 738,290 738,203 457,203]]\" ]\r\n" + 
					"}";
			
			JAXBContext jaxbContext = JAXBContext.newInstance(JaxbList.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			//marshaller.setProperty("eclipselink.media-type", "application/json");
			
			ObjectMapper mapper = new ObjectMapper();
			

			mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			//mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
			phList = mapper.readValue(jsonParsStr, JaxbList.class);
			
			//PageHit ph1 = mapper.readValue(phList.getList().get(0), PageHit.class);
			
			for (PageHit ph : phList.getList()) {
				System.out.println("ph: " + (PageHit) ph);
			}
			
			
			
			JaxbList<PageHit> hits = JaxbUtils.unmarshalJson(jsonParsStr, JaxbList.class);
			System.out.println("size of list: " + hits.getList().size());
			
			System.out.println(JaxbUtils.unmarshalJson(jsonParsStr, JaxbList.class));
			
			System.out.println(JaxbUtils.marshalToJsonString(phList, false));
			System.out.println(GsonUtil.toJson(phList));
			System.out.println(GsonUtil.toJson(l));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}