package eu.transkribus.core.model.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

public class CostEstimationRepresentation {
	private double creditValue;
	private Integer colId;
	private Integer modelId;
	private int nrOfPages;
	private Integer nrOfChars;
	private String docType;
	private String jobImpl;
	private String selectionStrategy;
	private List<TrpCreditPackage> chargedPackages;
	private boolean costsCovered;
	private String message;
	@XmlTransient
	private List<TrpCreditPackage> qualifyingPackages;
	@XmlTransient
	private TrpCreditTransaction transaction;
	public CostEstimationRepresentation() {
		chargedPackages = new ArrayList<>();
	}
	public double getCreditValue() {
		return creditValue;
	}
	public void setCreditValue(double creditValue) {
		this.creditValue = creditValue;
	}
	public Integer getColId() {
		return colId;
	}
	public void setColId(Integer colId) {
		this.colId = colId;
	}
	public Integer getModelId() {
		return modelId;
	}
	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}
	public int getNrOfPages() {
		return nrOfPages;
	}
	public void setNrOfPages(int nrOfPages) {
		this.nrOfPages = nrOfPages;
	}
	public Integer getNrOfChars() {
		return nrOfChars;
	}
	public void setNrOfChars(Integer nrOfChars) {
		this.nrOfChars = nrOfChars;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getJobImpl() {
		return jobImpl;
	}
	public void setJobImpl(String jobImpl) {
		this.jobImpl = jobImpl;
	}
	public String getSelectionStrategy() {
		return selectionStrategy;
	}
	public void setSelectionStrategy(String selectionStrategy) {
		this.selectionStrategy = selectionStrategy;
	}
	public List<TrpCreditPackage> getChargedPackages() {
		return chargedPackages;
	}
	public void setChargedPackages(List<TrpCreditPackage> chargedPackages) {
		this.chargedPackages = chargedPackages;
	}
	public boolean isCostsCovered() {
		return costsCovered;
	}
	public void setCostsCovered(boolean costsCovered) {
		this.costsCovered = costsCovered;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@XmlTransient
	public List<TrpCreditPackage> getQualifyingPackages() {
		return qualifyingPackages;
	}
	@XmlTransient
	public void setQualifyingPackages(List<TrpCreditPackage> qualifyingPackages) {
		this.qualifyingPackages = qualifyingPackages;
	}
	@XmlTransient
	public TrpCreditTransaction getTransaction() {
		return transaction;
	}
	@XmlTransient
	public void setTransaction(TrpCreditTransaction t) {
		this.transaction = t;
	}
}