package eu.transkribus.core.model.beans.searchresult;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SolrFieldValues {

    @XmlElement(name="vs")
    List<String> values;

    public SolrFieldValues(){}

    public void setValues(List<String> values){
        this.values = values;
    }

    public List<String> getValues(){
        return values;
    }
    
}
