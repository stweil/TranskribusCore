package eu.transkribus.core.model.beans;

import java.util.Objects;

public class StructureTrainValidationStats {
    private TrpStructureStats trainStats;
    private TrpStructureStats validationStats;

    public StructureTrainValidationStats() {};

    public TrpStructureStats getTrainStats() {
        return trainStats;
    }

    public void setTrainStats(TrpStructureStats trainStats) {
        this.trainStats = trainStats;
    }

    public TrpStructureStats getValidationStats() {
        return validationStats;
    }

    public void setValidationStats(TrpStructureStats validationStats) {
        this.validationStats = validationStats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StructureTrainValidationStats)) return false;
        StructureTrainValidationStats that = (StructureTrainValidationStats) o;
        return Objects.equals(getTrainStats(), that.getTrainStats()) && Objects.equals(getValidationStats(), that.getValidationStats());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTrainStats(), getValidationStats());
    }

    @Override
    public String toString() {
        return "StructureTrainValidationStats{" +
                "trainStats=" + trainStats +
                ", validationStats=" + validationStats +
                '}';
    }
}
