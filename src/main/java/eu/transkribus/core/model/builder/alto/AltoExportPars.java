package eu.transkribus.core.model.builder.alto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AltoExportPars {
	
	public static final String PARAMETER_KEY = "altoPars";
	
	boolean splitIntoWordsInAltoXml = true;
	boolean createAltov2 = false;
	boolean exportTextStyleTags = true;
	
	public AltoExportPars() {
		
	}
	
	public AltoExportPars(boolean splitIntoWordsInAltoXml) {
		super();
		this.splitIntoWordsInAltoXml = splitIntoWordsInAltoXml;
	}
	
	public AltoExportPars(boolean splitIntoWordsInAltoXml, boolean createAltoV2) {
		super();
		this.splitIntoWordsInAltoXml = splitIntoWordsInAltoXml;
		this.createAltov2 = createAltoV2;
	}

	public AltoExportPars(boolean splitIntoWordsInAltoXml, boolean createAltoV2, boolean exportTextStyleTags) {
		super();
		this.splitIntoWordsInAltoXml = splitIntoWordsInAltoXml;
		this.createAltov2 = createAltoV2;
		this.exportTextStyleTags = exportTextStyleTags;
	}

	public boolean isSplitIntoWordsInAltoXml() {
		return splitIntoWordsInAltoXml;
	}

	public void setSplitIntoWordsInAltoXml(boolean splitIntoWordsInAltoXml) {
		this.splitIntoWordsInAltoXml = splitIntoWordsInAltoXml;
	}

	public boolean isCreateAltov2() {
		return createAltov2;
	}

	public void setCreateAltov2(boolean createAltov2) {
		this.createAltov2 = createAltov2;
	}

	@Override
	public String toString() {
		return "AltoExportPars [splitIntoWordsInAltoXml=" + splitIntoWordsInAltoXml + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (splitIntoWordsInAltoXml ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AltoExportPars other = (AltoExportPars) obj;
		if (splitIntoWordsInAltoXml != other.splitIntoWordsInAltoXml)
			return false;
		return true;
	}

	public boolean isExportTextStyleTags() {
		return exportTextStyleTags;
	}

	public void setExportTextStyleTags(boolean exportTextStyleTags) {
		this.exportTextStyleTags = exportTextStyleTags;
	}
}
