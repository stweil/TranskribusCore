package eu.transkribus.core.model.builder.ms;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.util.Units;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dea.fimgstoreclient.FimgStoreGetClient;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.TrpFimgStoreConf;
import eu.transkribus.core.io.LocalDocReader;
import eu.transkribus.core.model.beans.JAXBPageTranscript;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpFImagestore;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.beans.TrpTranscriptMetadata;
import eu.transkribus.core.model.beans.pagecontent_trp.ITrpShapeType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextRegionType;
import eu.transkribus.core.model.builder.ExportCache;
import eu.transkribus.core.model.builder.NoTablesException;



public class TrpXlsxWordSnippetsExport {
	private final static Logger logger = LoggerFactory.getLogger(TrpXlsxWordSnippetsExport.class);
	Workbook wb;
	FimgStoreGetClient imgStoreClient;
	int currHeight;
	int currWidth;
	int rowCount = 0;
	
	CellStyle style=null;
	CellStyle rowStyle = null;
	CellStyle hLinkStyle = null;
	
	/*
	 * this keeps the names of the colums (=structure types) and its column index in the sheet
	 */
	Map<String, Integer> colNamesMap =  new HashMap<>();
	
	//old call for backward compatibility
	public void writeXlsxForShapes(TrpDoc doc, File exportFile, Set<Integer> pageIndices, IProgressMonitor monitor, ExportCache cache, boolean wordLayer, List<String> structNames, boolean withImages, boolean exportStructuresAsTable) throws NoTablesException, IOException, InterruptedException {
		writeXlsxForShapes(doc, exportFile, pageIndices, monitor, cache, wordLayer, structNames, withImages, exportStructuresAsTable, false);
	}
	

	/*
	 * this is used to export either all 'Fields' (which is more useful) or
	 * all lines - or if wanted words - into a table; if line/word export then sheets get created in an alphabetical style (e.g. all words starting with A in sheet A, ...
	 */
	/**
	 * @param doc to be exported
	 * @param exportFile: export file location and name
	 * @param pageIndices: page numbers to be exported
	 * @param monitor: progress monitor
	 * @param cache
	 * @param wordLayer: if word layer gets exported or lines
	 * @param structNames: list of structure types to export; if null all types are exported
	 * @param withImages: include image snippets of words or lines / not for structure types foreseen 
	 * @param exportStructuresAsTable: either export shape content or structure resp. Field content
	 * @param exportStructuresRowBased: either export structures in columns (each type is one dedicated column) or each region gets one row
	 * @throws NoTablesException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void writeXlsxForShapes(TrpDoc doc, File exportFile, Set<Integer> pageIndices, IProgressMonitor monitor, ExportCache cache, boolean wordLayer, List<String> structNames, boolean withImages, boolean exportStructuresAsTable, boolean exportStructuresRowBased) throws NoTablesException, IOException, InterruptedException {
		//TrpTableRegionType is contained in the regions too
		TrpFImagestore storeConfig = TrpFimgStoreConf.getFImagestore();
		imgStoreClient = new FimgStoreGetClient(storeConfig);

		List<TrpPage> pages = doc.getPages();
		String exportPath = exportFile.getPath();
		
		String basename = FilenameUtils.getBaseName(exportFile.getName());
		String extension = FilenameUtils.getExtension(exportFile.getName());
		String parentPath = exportFile.getParentFile().getPath();
		File currExportFile = null;
		exportPath = parentPath + "/" + basename + "." + extension;
		
		int totalPages = pageIndices==null ? pages.size() : pageIndices.size();
		if (monitor!=null) {
			monitor.beginTask("Exporting content of shapes to Excel", totalPages);
		}
		
		/*
		 * ToDo: extend a xlsx if it already exists!!!!!!
		 */
		currExportFile = new File(exportPath);
		if (currExportFile.exists()) {
			try {
				logger.debug("open the Excel file");
				// for getting the information of the file
				FileInputStream fip = new FileInputStream(currExportFile);

				// Getting the workbook instance for XLSX file
				wb = new XSSFWorkbook(fip);
				//wb = new XSSFWorkbook(exportPath);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else {
			wb = new XSSFWorkbook();
		}

		style=wb.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setWrapText(true);
		
		rowStyle = (CellStyle) wb.createCellStyle();
		rowStyle.setWrapText(true);
		
		hLinkStyle = wb.createCellStyle();
		Font hlinkfont = wb.createFont();
		hlinkfont.setUnderline(Font.U_SINGLE);
		hlinkfont.setColor(IndexedColors.BLUE.index);
		hLinkStyle.setFont(hlinkfont);
		
		int c=0;
		int rowNr = -1;
		boolean isStored = false;
		//export structure if structNames are null (export all) or if the list of structure types is not empty
		boolean exportStructure = structNames == null || !structNames.isEmpty();

		
		for (int i=0; i<pages.size(); i++) {
			if (pageIndices!=null && !pageIndices.contains(i))
				continue;

			currExportFile = new File(exportPath);

			/*
			 * we store the xlsx file after every 19th page, after this create the wb new to 'refresh' and speed up! the export
			 */
			if (isStored) {
				isStored = false;
				FileInputStream fip = null;
				try {
					logger.debug("open the wb once more");
					// for getting the information of the file
					fip = new FileInputStream(currExportFile);

					// Getting the workbook instance for XLSX file
					//wb = new XSSFWorkbook();
					wb = new XSSFWorkbook(fip);
					//wb = new XSSFWorkbook(exportPath);
					style=wb.createCellStyle();
					style.setBorderBottom(BorderStyle.THIN);
					style.setBorderTop(BorderStyle.THIN);
					style.setBorderRight(BorderStyle.THIN);
					style.setBorderLeft(BorderStyle.THIN);
					style.setWrapText(true);
					
					rowStyle = (CellStyle) wb.createCellStyle();
					rowStyle.setWrapText(true);
					
					hLinkStyle = wb.createCellStyle();
					hlinkfont = wb.createFont();
					hlinkfont.setUnderline(Font.U_SINGLE);
					hlinkfont.setColor(IndexedColors.BLUE.index);
					hLinkStyle.setFont(hlinkfont);
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				finally {
					if (fip != null) {
						try {
							fip.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			else {
				logger.debug("go on with this wb: " + currExportFile.getPath());
			}

			
			if (monitor!=null) {
				if (monitor.isCanceled()) {
					throw new InterruptedException("Export was canceled by user");
//					logger.debug("Xlsx export cancelled!");
//					return;
				}
				monitor.subTask("Processing page "+(c+1));
			}
			
			TrpPage page = pages.get(i);
			//try to get previously loaded JAXB transcript
			JAXBPageTranscript tr = null;
			if(cache != null) {
				tr = cache.getPageTranscriptAtIndex(i);
			}
			if (tr == null){
				TrpTranscriptMetadata md = page.getCurrentTranscript();
				tr = new JAXBPageTranscript(md);
				tr.build();
			}			
			
			TrpPageType trpPage = tr.getPage();
			String imgUrl = null;
			if (page != null && page.getImage() != null && page.getImage().getUrl() != null) {
				imgUrl = page.getImage().getUrl().toString();
			}
			if (imgUrl == null && trpPage.getPcGtsType().getMetadata().getTranskribusMetadata() != null) {
				imgUrl = trpPage.getPcGtsType().getMetadata().getTranskribusMetadata().getImgUrl();
			}
			
			logger.debug("imgUrl " + imgUrl);
			logger.debug("writing xlsx for page "+(i+1)+"/"+doc.getNPages());
			
			String fname = null;
			boolean isFirstRegion = true;
			/*
			 * this also returns all text regions of tables (all table cells)
			 * the second call (uncommented) returns the regions inclusive the table but without the table cells 
			 */
			List<TrpTextRegionType> regions = trpPage.getTextRegions(true);
			//List<TrpRegionType> regions = trpPage.getRegions();
						
			for (int j=0; j<regions.size(); ++j) {
				TrpRegionType r = regions.get(j);
				
				if (r instanceof TrpTextRegionType){
					//
					TrpTextRegionType textRegion = (TrpTextRegionType) r;
					List<ITrpShapeType> allShapes = new ArrayList<ITrpShapeType>();
					
					if (!wordLayer) {
						allShapes.addAll(textRegion.getChildren(false));
					}
					else {
						allShapes.addAll(textRegion.getWords());
					}

					String structureType = null;
					if (textRegion.getStructure() != null && exportStructure) {
						logger.debug("structure type: " + textRegion.getStructure());
						structureType=textRegion.getStructure();
						//check if the structure type is chosen by the user
						if ( (!structureType.isEmpty() && !(structNames==null) && !structNames.contains(structureType)) || structureType.isEmpty()) {
							logger.debug("condition: do not add to table!");
							continue;
						}
					}
					
					Integer collId = doc.getCollection() != null ? doc.getCollection().getColId() : null;
					
					if (exportStructuresAsTable && !exportStructuresRowBased) {
						
						//ToDO: structureType can be null here - decide if we leave or export the region content in a 'no structure' column

						boolean firstPage = rowNr==-1;
						//the first page
						if (firstPage) {
							createTable_structures(textRegion, null, page, null, 0, structureType, true);
						}

						String text = "";
						for (ITrpShapeType entry : allShapes) {
							text += entry.getUnicodeText() + System.getProperty("line.separator");
						}
						text = text.trim();
						if (isFirstRegion) {
							rowNr = (rowNr==-1)? 1 : rowNr+1;
							fname = page.getImgFileName();
							isFirstRegion = false;
						}
						else {
							fname = null;
						}
						createTable_structures(textRegion, text, page, fname, rowNr, structureType, false);
						
					}
					else if (exportStructuresAsTable && exportStructuresRowBased) {
						
						boolean firstPage = rowNr==-1;
						//the first page
						if (firstPage) {
							createTable_structures_in_rows(null, page, null, 0, structureType, true);
						}

						String text = "";
						for (ITrpShapeType entry : allShapes) {
							text += entry.getUnicodeText() + System.getProperty("line.separator");
						}
						text = text.trim();
						rowNr = (rowNr==-1)? 1 : rowNr+1;
						fname = page.getImgFileName();

						createTable_structures_in_rows(text, page, fname, rowNr, structureType, false);
					}
					else {
					
						try {
			            	//last param: for export with more then 10 pages 'alphabetical' sheets are created
							createTable(r, allShapes, page, fname, imgUrl, collId, structureType, withImages, (pages.size()<=10));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.debug("region id " + r.getId());
							logger.debug("allShapes size " + allShapes.size());
							logger.debug("doc ID " + page.getDocId());
							logger.debug("file name " + fname);
							logger.debug("imgUrl " + imgUrl);
							logger.debug("collId " + collId);
							logger.debug("structureType " + structureType);
							logger.debug("bool: withImages " + withImages);
							e.printStackTrace();
						}
					}

				}

				if (monitor!=null) {
					monitor.worked(c);
				}
			}
			++c;
			
			//store after 19 pages or for last page
			if (i>0 && i%19 == 0 || i==(pages.size()-1)) {
				
				FileOutputStream fOut;
				fOut = new FileOutputStream(exportPath);
				wb.write(fOut);
				isStored = true;
				fOut.close();
				wb.close();
			}
		}
		
		if (currExportFile.exists()) {
			try {
				logger.debug("open the wb once more");
				// for getting the information of the file
				FileInputStream fip = new FileInputStream(currExportFile);

				// Getting the workbook instance for XLSX file
				wb = new XSSFWorkbook(fip);
				//wb = new XSSFWorkbook(exportPath);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
		
		//so that cells are readable when opening
		XlsxUtils.autosizeTable(wb);

		FileOutputStream fOut;
		try {
			//means no tables at all
			if (wb.getNumberOfSheets() == 0){
				//throw new NoTablesException("Sorry - No tables available for export");
				logger.info("Sorry - export not possible");
				Sheet noTables = wb.createSheet(WorkbookUtil.createSafeSheetName("No content found"));
				CreationHelper crHelper = wb.getCreationHelper();
				Row firstRow = noTables.createRow(0);
				firstRow.createCell(0).setCellValue(crHelper.createRichTextString("Sorry - there were no words or structures available for your export. Please check the transcripts!"));
			}
			fOut = new FileOutputStream(exportPath);
			wb.write(fOut);
			fOut.close();
		} catch (IOException e) {
			if (!(e instanceof NoTablesException)) {
				logger.error(e.getMessage(), e);
			}
			throw e;
		}
		logger.info("wrote xlsx to: "+ exportPath);
	}
	
	/*
	 * strucureType is used for the sheet name
	 * so all words/lines end up in the sheet of there structure type
	 */
	private void createTable(TrpRegionType r, List<ITrpShapeType> allShapes, TrpPage page, String filename, String imgUrl, Integer collId, String structureType, boolean withImages, boolean singleSheet){

		try {
			Sheet currSheet = null;
			//for each starting character -> extra sheet in xlsx
			
			if (structureType != null) {
				logger.debug("export to sheet: " + structureType);
				if (wb.getSheet(structureType) != null){
					currSheet = wb.getSheet(structureType);
				}
				else{
					currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(structureType));
				}
			}
			else if (singleSheet) {
				logger.debug("export to sheet: dataSheet");
				if (wb.getSheet("dataSheet") != null){
					currSheet = wb.getSheet("dataSheet");
				}
				else{
					currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName("dataSheet"));
				}
			}

			int colIdtmp = 0;
			
			String url = page.getUrl().toString();
			if (imgUrl != null) {
				url = imgUrl ;
			}
			String imgKey = StringUtils.substringBetween(url, "Get?id=", "&fileType=view");	
			
			byte[] imgByteArray;
			BufferedImage src = null;
			
			if (withImages) {
				try {
					imgByteArray = imgStoreClient.getImg(imgKey).getData();
			        // convert byte[] back to a BufferedImage
			        InputStream is = new ByteArrayInputStream(imgByteArray);
			        src = ImageIO.read(is);
			        is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.debug("io of image store ");
					e.printStackTrace();
				}
			}
			
			/*
			 * ToDO: create headings for first page if wanted
			 */

			String transcriptKey = page.getCurrentTranscript().getKey();
			String link2Lite = null;
			if (collId != null) {
				link2Lite = "https://beta.transkribus.eu/collection/"+collId+"/doc/"+page.getDocId()+"/detail/"+ page.getPageNr() +"?key="+transcriptKey+"&view=both";
			}

			for (ITrpShapeType entry : allShapes) {
				
				String text = entry.getUnicodeText();
				colIdtmp = 0;
				
				/*
				 * to export text with textStyle='strikethrough' as 'xxx'
				 * worked for a very special use case
				 * do not use in general 
				 */
//				List<CustomTag> textStyle = entry.getCustomTagList().getIndexedTags("textStyle");
//				if (!textStyle.isEmpty()) {
//					text = "xxx";
//				}

				//export into alphabetical sheets if all words are exported
				if (structureType == null && !singleSheet) {
			    	String startChar = text.length()>1 ? text.substring(0, 1) : "";
			    	String alphabetSheetName = "sheet_"+startChar;
			    	
					if (wb.getSheet(alphabetSheetName) != null){
						currSheet = wb.getSheet(alphabetSheetName);
					}
					else{
						currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(alphabetSheetName));
					}
				}

				currSheet.setColumnWidth(0, 10000);
				rowCount = currSheet.getLastRowNum();
			    	        
				Row nextRow = null;
				Cell currCell;

				CreationHelper helper;
				helper = wb.getCreationHelper();
				nextRow = currSheet.createRow(++rowCount);
				
				if (withImages) {
					logger.debug("Excel export with images");				
					nextRow.setRowStyle(rowStyle);
					currCell = nextRow.createCell(colIdtmp++);
					currCell.setCellValue(text);
					currCell.setCellStyle(style);
					String coords = entry.getCoordinates();
					int[] cropValues = XlsxUtils.getCropValues(coords, src);
					//logger.debug("crop values: " + cropValues);
					currHeight = cropValues[3];
					currWidth = cropValues[2];
					BufferedImage dest = src.getSubimage(cropValues[0], cropValues[1], cropValues[2], cropValues[3]);
					src.flush();
					byte[] bytes = null;
					try {
						bytes = XlsxUtils.compressImageToByteArray(dest, 0.20f);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

					}
					int inputImage = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
					bytes = null;
					XSSFDrawing drawing = (XSSFDrawing) currSheet.createDrawingPatriarch();
					
					ClientAnchor imgAnchor = helper.createClientAnchor();
					/*
					 * for sorting it is necessary that the setRow1 and setRow2 is the same row index and that
					 * the setDx1, setDx2,... are used!! beside using AnchorType.MOVE_AND_RESIZE
					 */
					imgAnchor.setCol1(0);
					imgAnchor.setCol2(0);
					imgAnchor.setRow1(nextRow.getRowNum());
					imgAnchor.setRow2(nextRow.getRowNum());
					imgAnchor.setAnchorType(AnchorType.MOVE_AND_RESIZE);
					float rowHeight = (float) Units.pixelToPoints(currHeight); // picture's height must fit into row height
					nextRow.setHeightInPoints(rowHeight);
					imgAnchor.setDx1(Units.toEMU(1));
					imgAnchor.setDy1(Units.toEMU(1));
					imgAnchor.setDx2(Units.toEMU(currWidth - 1));
					imgAnchor.setDy2(Units.toEMU(currHeight - 1));
					//do not resize pic -> sorting will not work
					Picture pic = drawing.createPicture(imgAnchor, inputImage);
				}
				else {
					//if no image is exported keep this column empty so taht the reimport will work
					currCell = nextRow.createCell(colIdtmp++);
			    	currCell.setCellValue("");   
			    	currCell.setCellStyle(style);
				}

				currCell = nextRow.createCell(colIdtmp++);
				currCell.setCellValue(text);   
				currCell.setCellStyle(style);
				
				currCell = nextRow.createCell(colIdtmp++);
				currCell.setCellValue(r.getId() + "/" + entry.getId());  
				currCell.setCellStyle(style);
				
				currCell = nextRow.createCell(colIdtmp++);
				currCell.setCellValue(page.getPageId());   
				currCell.setCellStyle(style);
				
				if (singleSheet && structureType != null) {
					currCell = nextRow.createCell(colIdtmp++);
			    	currCell.setCellValue(structureType);   
			    	currCell.setCellStyle(style);
				}

			    if (link2Lite != null) {
					org.apache.poi.ss.usermodel.Hyperlink link = helper.createHyperlink(HyperlinkType.URL);
				    link.setAddress(link2Lite);
				    link.setLabel(link2Lite);
				    
					currCell = nextRow.createCell(colIdtmp++);
					currCell.setCellValue(link2Lite);
					currCell.setHyperlink(link);
					currCell.setCellStyle(hLinkStyle);
			    }
				
//				String [] textlines = text.split(System.lineSeparator());
//				
//				if (textlines.length > 0){
//					int length = textlines.length;
//					if (maxLines < length){
//						maxLines = length;
//					}
//				}
  
			    /*
			     * set the height of a row manually since it does not work for merged region cells!!!
			     * so if there are merged cells in a row we remember the maximum number of text lines in one of these cells to set the appropriate height
			     */	
//			if (maxLines > 0){
//				logger.debug("row contains maximum nr.- of lines in cell of: "  + maxLines);
//				nextRow.setHeight((short) (nextRow.getHeight()*maxLines));
//			}
			}
		} catch (Exception e) {
			logger.debug("here is an exception...." + e.getLocalizedMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	/*
	 * toDo: test what happens if the structureType is null here - which can happen
	 * may we append the content in an 'unstructured' column
	 */
	private void createTable_structures(TrpRegionType r, String text, TrpPage page, String filename, int rowCount, String structureType, boolean isFirstRow){

		try {
			Sheet currSheet = null;
			logger.debug("export to sheet: dataSheet");
			if (wb.getSheet("dataSheet") != null){
				currSheet = wb.getSheet("dataSheet");
			}
			else{
				currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName("dataSheet"));
			}
			
			/*
			 * this is the part for adding the filename for each exported page
			 */
			if (colNamesMap.isEmpty()) {
				colNamesMap.put("TranskribusFilename", 0);
				colNamesMap.put("pageNr", 1);
			}
			Row firstRow = currSheet.getRow(0);
			if (firstRow == null) {
				firstRow = currSheet.createRow(0);
			}
			if (isFirstRow) {
				Cell firstCell = firstRow.createCell(0);
				firstCell.setCellValue("TranskribusFilename");   
				firstCell.setCellStyle(style);
				
				//add page number as well
				Cell secondCell = firstRow.createCell(1);
				secondCell.setCellValue("pageNr");   
				secondCell.setCellStyle(style);
			}

			
			/*
			 * after adding the filename and pageNr all structure regions are added -> idx starts at 2
			 * in the first row: each structure type
			 * in the following rows: each row contains all entries of a single page
			 */
			int colIdtmp = 2;
			boolean addToFirstRow = isFirstRow;
			if  (colNamesMap.containsKey(structureType)) {
				colIdtmp = colNamesMap.get(structureType);
			}	
			else {
				int idx = colNamesMap.keySet().size();
				colNamesMap.put(structureType, idx);
				colIdtmp = idx;
				addToFirstRow = true;
			}
			        
			Row nextRow = currSheet.getRow(rowCount);
			Cell currCell;

			if (nextRow == null) {
				nextRow = currSheet.createRow(rowCount);
				nextRow.setRowStyle(rowStyle);
			}
			
			if (filename != null) {
				currCell = nextRow.getCell(0);
				if (currCell == null) {
					currCell = nextRow.createCell(0);
				}
				currCell.setCellValue(filename);   
				currCell.setCellStyle(style);
			}
				
			if (page != null && !isFirstRow) {
				//second column -> page number
				currCell = nextRow.getCell(1);
				if (currCell == null) {
					currCell = nextRow.createCell(1);
				}
				currCell.setCellValue(page.getPageNr());   
				currCell.setCellStyle(style);
			}
			
			if (addToFirstRow && structureType != null) {
				currCell = firstRow.createCell(colIdtmp);
		    	currCell.setCellValue(structureType);   
		    	currCell.setCellStyle(style);
			}
			
			if (!isFirstRow){
				currCell = nextRow.getCell(colIdtmp);
				if (currCell == null) {
					currCell = nextRow.createCell(colIdtmp);
				}
				String text2Add = currCell.getStringCellValue().isEmpty() ? text : currCell.getStringCellValue() + System.getProperty("line.separator") + text;
				currCell.setCellValue(text2Add);   
				currCell.setCellStyle(style);
			}
			
		} catch (Exception e) {
			logger.debug("here is an exception...." + e.getLocalizedMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	
	/*
	 * each field becomes a single row with filename, page namber, structure type and content
	 */
	private void createTable_structures_in_rows(String text, TrpPage page, String filename, int rowCount, String structureType, boolean isFirstRow){

		try {
			Sheet currSheet = null;
			logger.debug("export to sheet: dataSheet");
			if (wb.getSheet("dataSheet") != null){
				currSheet = wb.getSheet("dataSheet");
			}
			else{
				currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName("dataSheet"));
			}
			
			Row firstRow = currSheet.getRow(0);
			if (firstRow == null) {
				firstRow = currSheet.createRow(0);
			}
			if (isFirstRow) {
				Cell firstCell = firstRow.createCell(0);
				firstCell.setCellValue("TranskribusFilename");   
				firstCell.setCellStyle(style);
				
				Cell secondCell = firstRow.createCell(1);
				secondCell.setCellValue("pageNr");   
				secondCell.setCellStyle(style);
				
				Cell thirdCell = firstRow.createCell(2);
				thirdCell.setCellValue("structureType");   
				thirdCell.setCellStyle(style);
				
				Cell forthCell = firstRow.createCell(3);
				forthCell.setCellValue("content");   
				forthCell.setCellStyle(style);
			}
			else {
				Row nextRow = currSheet.getRow(rowCount);
				Cell currCell;

				if (nextRow == null) {
					nextRow = currSheet.createRow(rowCount);
					nextRow.setRowStyle(rowStyle);
				}
				
				if (filename != null) {
					currCell = nextRow.getCell(0);
					if (currCell == null) {
						currCell = nextRow.createCell(0);
					}
					currCell.setCellValue(filename);   
					currCell.setCellStyle(style);
				}
					
				if (page != null) {
					//second column -> page number
					currCell = nextRow.getCell(1);
					if (currCell == null) {
						currCell = nextRow.createCell(1);
					}
					currCell.setCellValue(page.getPageNr());   
					currCell.setCellStyle(style);
				}
				
				if (structureType != null) {
					currCell = nextRow.getCell(2);
					if (currCell == null) {
						currCell = nextRow.createCell(2);
					}
					currCell.setCellValue(structureType);   
					currCell.setCellStyle(style);
				}
							
				if (text != null){
					currCell = nextRow.getCell(3);
					if (currCell == null) {
						currCell = nextRow.createCell(3);
					}
					currCell.setCellValue(text);   
					currCell.setCellStyle(style);
				}
			}
			        			
		} catch (Exception e) {
			logger.debug("here is an exception...." + e.getLocalizedMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	


	public static void main(String[] args) throws Exception {
		
//		convertXslxIntoCsv("Y:/DIG_auftraege_archiv/tmp/StazH/match_xslx");
		
		
		TrpDoc docWithoutTables = LocalDocReader.load("/mnt/TRAINING_VALIDATION_SET_Filae_firstnames/");
		//TrpDoc docWithTables = LocalDocReader.load("fullpath");
		
		Set<Integer> pageIndices = null; // null means every page
		
		//pageIndices must be set here instead of being null because it gets used in ExportUtils
		if (pageIndices == null){
			pageIndices = new HashSet<Integer>();
			for (int i = 0; i<docWithoutTables.getNPages(); i++){
				pageIndices.add(i);
			}
		}
//		DocExporter docExporter =  new DocExporter();
//		docExporter.getCache().storePageTranscripts4Export(docWithoutTables, pageIndices, null, "Latest", -1, null);
		
		TrpXlsxWordSnippetsExport txslx = new TrpXlsxWordSnippetsExport();
		txslx.writeXlsxForShapes(docWithoutTables, new File("destPath"), pageIndices, null, null, false, null, false, false);
		
	}

}
