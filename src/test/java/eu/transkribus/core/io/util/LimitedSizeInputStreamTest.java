package eu.transkribus.core.io.util;

import eu.transkribus.core.exceptions.FileSizeExceededException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LimitedSizeInputStreamTest {
	private static final Logger logger = LoggerFactory.getLogger(LimitedSizeInputStreamTest.class);
	
	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder();
	
	@Test
	public void testLimit() throws IOException {
		File tmp = tempDir.newFile("streamSource.txt");
		final int testSize = 1024;
		String str = StringUtils.rightPad("", testSize, 'a');
		FileUtils.writeStringToFile(tmp, str);
		File tmpOut = tempDir.newFile("out.txt");
		int sizeLimit = testSize - 10;
		
		try(FileInputStream fis = new FileInputStream(tmp);
				InputStream is = new LimitedSizeInputStream(fis, sizeLimit);
				OutputStream os = new FileOutputStream(tmpOut);) {
			byte[] data = new byte[1];
			int nrOfBytes;
			while((nrOfBytes = is.read(data)) != -1){
				os.write(data, 0, nrOfBytes);
			}
			os.flush();		
		} catch(FileSizeExceededException e) {
			logger.info("Exceeded filesize limit of {} byte. Input size = {} byte, Output size = {} byte", 
					sizeLimit, tmp.length(), tmpOut.length());
		}
	}
}
