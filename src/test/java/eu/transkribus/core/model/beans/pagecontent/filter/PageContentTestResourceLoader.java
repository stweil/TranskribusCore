package eu.transkribus.core.model.beans.pagecontent.filter;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBException;

import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.PageXmlUtils;

/**
 * Utility for loading page XMLs from the PageContentFilter test resource path
 */
class PageContentTestResourceLoader {
	PcGtsType loadXmlResource(String name) throws JAXBException, IOException {
		PcGtsType pc;
		try (InputStream is = this.getClass().getClassLoader()
				.getResourceAsStream("PageContentFilter/" + name)) {
			pc = PageXmlUtils.unmarshal(is);
		}
		return pc;
	}
}
