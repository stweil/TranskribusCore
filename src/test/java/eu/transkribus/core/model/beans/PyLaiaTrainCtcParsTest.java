package eu.transkribus.core.model.beans;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class PyLaiaTrainCtcParsTest {

    @Test
    public void testGetNextGenConf() throws Exception {
//		PyLaiaTrainCtcPars p = PyLaiaTrainCtcPars.getDefault();
//		System.out.println(p.toSingleLineString());
		// String str = "--max_nondecreasing_epochs 20 --max_epochs 250 --batch_size 24 --learning_rate 3.0E-4 --delimiters <SPACE> --use_baidu_ctc True --add_logsoftmax_to_loss False --train_path ./model --logging_level info --logging_also_to_stderr info --logging_file train-crnn.log --show_progress_bar False --use_distortions True --print_args True";
		// PyLaiaTrainCtcPars p = PyLaiaTrainCtcPars.fromSingleLineString(str);
		// logger.info("p = "+p.toSingleLineString());
		PyLaiaTrainCtcPars p = PyLaiaTrainCtcPars.getDefault();
		InputStream in = p.getClass().getClassLoader().getResourceAsStream("train_conf.yaml");
		String baseConfString = IOUtils.toString(in, StandardCharsets.UTF_8);
		System.out.println("baseConfString: "+baseConfString);

		p.addParameter("--checkpoint", "i_am_a_checkpoint.ckpt");
		String confString = p.getNextGenConf(baseConfString);
		System.out.println("confString: \n"+confString);
    }

	public static void main(String[] args) throws Exception {
        PyLaiaTrainCtcParsTest t = new PyLaiaTrainCtcParsTest();
        t.testGetNextGenConf();
	}
}
