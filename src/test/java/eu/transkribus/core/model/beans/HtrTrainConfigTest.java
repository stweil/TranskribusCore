package eu.transkribus.core.model.beans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.DocumentSelectionDescriptor.PageDescriptor;
import eu.transkribus.core.model.mappers.TrpModelMetadataMapper;
import eu.transkribus.core.util.JaxbUtils;
import eu.transkribus.core.util.ModelUtil;

public class HtrTrainConfigTest {
	private static final Logger logger = LoggerFactory.getLogger(HtrTrainConfigTest.class);
	
	public static void main(String[] args) throws JAXBException, IOException {
		PyLaiaHtrTrainConfig htc = createPyLaiaConfig();
		
//		System.out.println(JaxbUtils.marshalToString(htc, DocumentSelectionDescriptor.class, PageDescriptor.class));
		Properties props = new Properties();
		props.put("test", JaxbUtils.marshalToString(htc, DocumentSelectionDescriptor.class, PageDescriptor.class));
		props.put("test2", "test2");
		props.store(new FileOutputStream(new File("/tmp/test.properties")), null);

		Properties props2 = new Properties();
		props2.load(new FileReader(new File("/tmp/test.properties")));
			
		String objectStr = (String)props2.get("test");
		PyLaiaHtrTrainConfig config2 = JaxbUtils.unmarshal(objectStr, PyLaiaHtrTrainConfig.class, DocumentSelectionDescriptor.class, PageDescriptor.class);
		System.out.println(config2.getModelMetadata().getDescriptions().get(Locale.ENGLISH.getLanguage()));
	}
	
	private static TrpModelMetadata createMetadata() {
		TrpModelMetadata md = new TrpModelMetadata();
		Map<String, String> descs = new HashMap<>();
		descs.put(Locale.ENGLISH.getLanguage(), "A description");
		md.setDescriptions(descs);
		md.setLanguage("German");
		md.setName("Test Model");
		md.setBaseModelId(123);
		return md;
	}

	@Deprecated
	private static CitLabHtrTrainConfig createConfig() {
		CitLabHtrTrainConfig htc = new CitLabHtrTrainConfig();
		
		htc.setColId(2);
		TrpModelMetadata md = createMetadata();
		htc.setModelMetadata(md);
		htc.setLearningRate("2e-3");
		htc.setNoise("both");
		htc.setNumEpochs(200);
		htc.setTrainSizePerEpoch(1000);
		
		for(int i = 1; i < 3; i++) {
			DocumentSelectionDescriptor d = new DocumentSelectionDescriptor();
			d.setDocId(i);
			for(int j = 1; j < 3; j++) {
				PageDescriptor p = new PageDescriptor();
				p.setPageId(j);
				p.setTsId(j);
				d.getPages().add(p);
			}
			htc.getTrain().add(d);
		}
		return htc;
	}

	@Deprecated
	private static CitLabHtrTrainConfig createPlusConfig() {
		CitLabHtrTrainConfig htc = new CitLabHtrTrainConfig();
		
		htc.setColId(2);
		TrpModelMetadata md = createMetadata();
		md.setProvider(ModelUtil.PROVIDER_CITLAB_PLUS);
		htc.setModelMetadata(md);
		htc.setLearningRate("2e-3");
		htc.setNoise("both");
		htc.setNumEpochs(200);
		htc.setTrainSizePerEpoch(1000);
		htc.setCustomParam("testParam", "testParamValue");
		
		for(int i = 1; i < 3; i++) {
			DocumentSelectionDescriptor d = new DocumentSelectionDescriptor();
			d.setDocId(i);
			for(int j = 1; j < 3; j++) {
				PageDescriptor p = new PageDescriptor();
				p.setPageId(j);
				p.setTsId(j);
				d.getPages().add(p);
			}
			htc.getTrain().add(d);
		}
		return htc;
	}

	private static PyLaiaHtrTrainConfig createPyLaiaConfig() {
		PyLaiaHtrTrainConfig htc = new PyLaiaHtrTrainConfig();

		htc.setColId(2);
		TrpModelMetadata md = createMetadata();
		md.setProvider(ModelUtil.PROVIDER_PYLAIA);
		htc.setModelMetadata(md);
		htc.setLearningRate(0.002);
		htc.setNumEpochs(200);
		htc.setCustomParam("testParam", "testParamValue");

		for(int i = 1; i < 3; i++) {
			DocumentSelectionDescriptor d = new DocumentSelectionDescriptor();
			d.setDocId(i);
			for(int j = 1; j < 3; j++) {
				PageDescriptor p = new PageDescriptor();
				p.setPageId(j);
				p.setTsId(j);
				d.getPages().add(p);
			}
			htc.getTrain().add(d);
		}
		return htc;
	}

	
	@Test
	public void testConfig() throws JAXBException {
		PyLaiaHtrTrainConfig htc = createPyLaiaConfig();
		
		logger.info(JaxbUtils.marshalToString(htc, DocumentSelectionDescriptor.class, PageDescriptor.class));
		logger.info(JaxbUtils.marshalToJsonString(htc, true));
	}
	
	@Test
	public void testConfigWithModelMetadata() throws JAXBException {
		PyLaiaHtrTrainConfig htc = createPyLaiaConfig();
		
		logger.info(JaxbUtils.marshalToJsonString(htc, false));
		
		logger.debug("Config = {}", htc);
		
		Assert.assertNotNull(htc.getModelMetadata());
		Assert.assertNotNull(htc.getModelMetadata().getName());
		Assert.assertNotNull(htc.getModelMetadata().getLanguage());
		Assert.assertNotNull(htc.getModelMetadata().getDescriptions().get(Locale.ENGLISH.getLanguage()));
		Assert.assertNotNull(htc.getModelMetadata().getProvider());
	}

	@Test
	public void testPyLaiaConfigWithModelMetadata() throws JAXBException {
		PyLaiaHtrTrainConfig htc = createPyLaiaConfig();

		logger.info("\n" + JaxbUtils.marshalToJsonString(htc, false));

		logger.debug("Config = {}", htc);

		Assert.assertNotNull(htc.getModelMetadata());
		Assert.assertNotNull(htc.getModelMetadata().getName());
		Assert.assertNotNull(htc.getModelMetadata().getLanguage());
		Assert.assertNotNull(htc.getModelMetadata().getDescriptions().get(Locale.ENGLISH.getLanguage()));
		Assert.assertNotNull(htc.getModelMetadata().getProvider());
	}
	
	@Test
	public void testConfigWithModelMetadataMarshalling() throws JAXBException {
		//json including the old name, language and description fields. This is what an old client would send to the server
		final String json = "{\"type\":\"citLabHtrTrainConfig\","
				+ "\"modelName\":\"Test Model\",\"language\":\"German\",\"description\":\"A description\","
				+ "\"colId\":2,\"provider\":\"CITlabPlus\",\"customParams\":{\"entry\":[{\"key\":\"testParam\",\"value\":\"testParamValue\"}]},\"trainList\":{\"train\":[{\"docId\":1,\"pageList\":{\"pages\":[{\"pageId\":1,\"tsId\":1,\"regionIds\":[]},{\"pageId\":2,\"tsId\":2,\"regionIds\":[]}]}},{\"docId\":2,\"pageList\":{\"pages\":[{\"pageId\":1,\"tsId\":1,\"regionIds\":[]},{\"pageId\":2,\"tsId\":2,\"regionIds\":[]}]}}]},\"testList\":{\"test\":[]},\"numEpochs\":200,\"learningRate\":\"2e-3\",\"noise\":\"both\",\"trainSizePerEpoch\":1000}";
		CitLabHtrTrainConfig htc = JaxbUtils.unmarshalJson(json, CitLabHtrTrainConfig.class);
		
		logger.info("Metadata name: {}", htc.getModelMetadata().getName());
		logger.info("Config = {}", htc);
		
		
		Assert.assertNotNull(htc.getModelMetadata());
		Assert.assertNotNull(htc.getModelMetadata().getName());
		Assert.assertNotNull(htc.getModelMetadata().getLanguage());
		Assert.assertNotNull(htc.getModelMetadata().getDescriptions().get(Locale.ENGLISH.getLanguage()));
		Assert.assertNotNull(htc.getModelMetadata().getProvider());
	}
	
	@Test
	public void testConfigWithModelMetadataMapping() throws JAXBException {
		//json including the old name, language and description fields. This is what an old client would send to the server
		final String json = "{\"type\":\"citLabHtrTrainConfig\","
				+ "\"modelName\":\"Test Model\",\"language\":\"German\",\"description\":\"A description\","
				+ "\"colId\":2,\"baseModelId\":123,\"provider\":\"CITlabPlus\",\"customParams\":{\"entry\":[{\"key\":\"testParam\",\"value\":\"testParamValue\"}]},\"trainList\":{\"train\":[{\"docId\":1,\"pageList\":{\"pages\":[{\"pageId\":1,\"tsId\":1,\"regionIds\":[]},{\"pageId\":2,\"tsId\":2,\"regionIds\":[]}]}},{\"docId\":2,\"pageList\":{\"pages\":[{\"pageId\":1,\"tsId\":1,\"regionIds\":[]},{\"pageId\":2,\"tsId\":2,\"regionIds\":[]}]}}]},\"testList\":{\"test\":[]},\"numEpochs\":200,\"learningRate\":\"2e-3\",\"noise\":\"both\",\"trainSizePerEpoch\":1000}";
		CitLabHtrTrainConfig htc = JaxbUtils.unmarshalJson(json, CitLabHtrTrainConfig.class);
		
		TrpHtr htr = TrpModelMetadataMapper.fromRepresentation(htc.getModelMetadata());
		logger.info("Mapped modelMetadata to internal representation = {}", htr);
		
		Assert.assertNotNull(htr.getName());
		Assert.assertNotNull(htr.getDescription());
		Assert.assertNotNull(htr.getLanguage());
		Assert.assertNotNull(htr.getBaseHtrId());
	}
}
