package eu.transkribus.core.model.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.transkribus.core.model.beans.job.enums.JobImpl;

import eu.transkribus.core.util.JacksonUtil;
import org.junit.Assert;
import org.junit.Test;

public class LaConfigTest {

    @Test
    public void testJsonMarshalling() throws JsonProcessingException {
        LaConfig source = new LaConfig(JobImpl.TranskribusLaJob);
        source.setOverwriteExistingLayout(true);
        source.setDoBlockSeg(true);
        source.setDoLineSeg(true);
        source.setDoWordSeg(true);
        source.setDoBaselineToPolygon(true);
        source.setDoPolygonToBaseline(false);
        source.setB2pBackend("Legacy");
        source.getProps().setProperty("pars.clustering_legacy_type", "default");
        source.getProps().setProperty("pars.clustering_method", "legacy");

        //marshal to String as LocalRecognitionJobManager writes the config to job props like this
        String laConfigJsonStr = JacksonUtil.toJson(source);
        //unmarshal like PyLaiaDecodingJob would do
        LaConfig result = JacksonUtil.fromJson(laConfigJsonStr, LaConfig.class);
        Assert.assertEquals(source, result);
    }
}
