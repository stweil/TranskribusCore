package eu.transkribus.core.model.beans.pagecontent.filter;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;

import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.PageXmlUtils;

public class PageContentFilterTest {
	PageContentTestResourceLoader loader = new PageContentTestResourceLoader();
	
	@Test
	public void testNullFilterChain() throws JAXBException, IOException {
		PcGtsType pc = loadTaggedXml();
		
		final String xmlStringBefore = new String(PageXmlUtils.marshalToBytes(pc));
		
		IPageContentFilter filterChain = new PageContentFilterChain();
		filterChain.doFilter(pc);
		
		final String xmlStringAfter = new String(PageXmlUtils.marshalToBytes(pc));
		/**
		 * Check disabled as there is no way to check equality of a PAGE XML.
		 * readingOrder will be rewritten on marshalling!
		 */
//		Assert.assertEquals(xmlStringBefore, xmlStringAfter);
	}
	
	@Test
	public void testTagPageContentFilterGap() throws JAXBException, IOException {
		//should remove 1 line
		testTagPageContentFilter(loadTaggedXml(), 1, "gap");
	}
	
	@Test
	public void testTagPageContentFilterUnclear() throws JAXBException, IOException {
		//should remove 1 line
		testTagPageContentFilter(loadTaggedXml(), 1, "unclear");
	}
	
	@Test
	public void testTagPageContentFilterGapAndUnclear() throws JAXBException, IOException {
		//should remove 2 lines
		testTagPageContentFilter(loadTaggedXml(), 2, "gap", "unclear");
	}
	
	@Test
	public void testTagPageContentFilterNonsenseTag() throws JAXBException, IOException {
		//should do nothing
		testTagPageContentFilter(loadTaggedXml(), 0, "asdfg");
	}
	
	@Test
	public void testTagPageContentFilterNoTagsSpecified() throws JAXBException, IOException {
		//should do nothing
		testTagPageContentFilter(loadTaggedXml(), 0);
	}
	
	public void testTagPageContentFilter(PcGtsType pc, int expectedLinesRemoved, String...tags) throws JAXBException, IOException {
		TagPageContentFilter tagFilter = new TagPageContentFilter(tags);
		final int nrOfLinesInitial = PageXmlUtils.getLines(pc).size();
		tagFilter.doFilter(pc);
		final int nrOfLinesAfterFilter = PageXmlUtils.getLines(pc).size();
		Assert.assertEquals(expectedLinesRemoved, tagFilter.getRemovedLinesCount());
		Assert.assertEquals(nrOfLinesInitial - expectedLinesRemoved, nrOfLinesAfterFilter);
		
	}
	
	/**
	 * @return a PcGtsType with one line containing a gap tag and one line containing an unclear tag
	 * @throws JAXBException
	 * @throws IOException
	 */
	private PcGtsType loadTaggedXml() throws JAXBException, IOException {
		return loader.loadXmlResource("gapAndUnclearTaggedLines.xml");
	}
}
