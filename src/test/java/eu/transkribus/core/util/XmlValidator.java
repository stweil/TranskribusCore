package eu.transkribus.core.util;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class XmlValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(XmlValidator.class);
	
	public static boolean validateAltoFile(File file, File destFile) {
		try {
			URL schemaFile = new URL("https://www.loc.gov/standards/alto/v4/alto.xsd");
			// webapp example xsd: 
			//URL schemaFile = new URL("http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd");
			// local file example:
			// File schemaFile = new File("/location/to/localfile.xsd"); // etc.
			Source xmlFile = new StreamSource(destFile);
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			validator.validate(xmlFile);
			logger.debug(destFile.getName() + " is valid");
		} catch (SAXException e) {
			return false;

		} catch (IOException e) {}
		finally{
			//System.in.read();
			
		}
		return true;
	}
	
	public static boolean validatePageFile(File file, File destFile) {
		try {
			URL schemaFile = new URL(" http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd");
			// webapp example xsd: 
			//URL schemaFile = new URL("http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd");
			// local file example:
			// File schemaFile = new File("/location/to/localfile.xsd"); // etc.
			Source xmlFile = new StreamSource(destFile);
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			validator.validate(xmlFile);
			logger.debug(destFile.getName() + " is valid");
		} catch (SAXException e) {
			return false;

		} catch (IOException e) {}
		finally{
			//System.in.read();
			
		}
		return true;
	}
	

}
