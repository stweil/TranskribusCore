package eu.transkribus.core.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;

public class NaturalOrderComparatorTest {
	
	@Test
	public void testCompare() {
		//strings with equal length, but the split operation in NaturalOrderComparator will produce arrays of different length
//		String s1 = "69429%2Fc0000006rh8z";
//		String s2 = "69429%2Fc0000006rh08";
		String s1 = "1a";
		String s2 = "01";
		
		NaturalOrderComparator c = new NaturalOrderComparator();
		int result = c.compare(s1, s2);
		Assert.assertTrue("Comparator result must be positive", result > 0);
	}
	
	@Test
	public void testCompare2() {
		//strings with unequal length, but the split operation in NaturalOrderComparator will produce arrays of same length
		String s1 = "00100a01";
		String s2 = "100a01";
		
		NaturalOrderComparator c = new NaturalOrderComparator();
		int result = c.compare(s1, s2);
		Assert.assertTrue("Comparator result must be positive but is " + result, result > 0);
	}
	
	@Test
	public void testCompareNumbers() {
		String s1 = "100";
		String s2 = "2";
		
		NaturalOrderComparator c = new NaturalOrderComparator();
		int result = c.compare(s1, s2);
		Assert.assertTrue("Comparator result must be positive", result > 0);
	}
	
	public static void main(String[] args) {
		//	File dir = new File("/mnt/read_scratch/TRP/test/II._ZvS_1908_1.Q");
		File dir = new File("/mnt/read_scratch/TRP/test/Schauplatz_test");

		File[] imgs = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".jpg");
			}
		});

		Comparator<String> naturalOrderComp = new NaturalOrderComparator();

		Map<String, String> map = new TreeMap<>(naturalOrderComp);

		for (File i : imgs) {
			String name = FilenameUtils.getBaseName(i.getName());
			String nameComp = i.getName();
			map.put(name, nameComp);
		}

		for (Entry<String, String> e : map.entrySet()) {
			System.out.println(e.getKey() + " - " + e.getValue());
		}

	}
}
