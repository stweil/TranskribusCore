package eu.transkribus.core.util;

import org.junit.Ignore;
import org.junit.Test;

public class EmailMxLookupTest {
    @Ignore("Not sure if this would work on all machines where the test runs")
    @Test
    public void testLookup() {
        //run on addresses but don't assert - we don't know the result. There must not be an Exception.
        EMailMxLookup.doLookupAddress("user@readcoop.eu");
        EMailMxLookup.doLookupHost("asdasd.de");
        EMailMxLookup.doLookupHost("aaaareadcoop.eu");
    }
}
